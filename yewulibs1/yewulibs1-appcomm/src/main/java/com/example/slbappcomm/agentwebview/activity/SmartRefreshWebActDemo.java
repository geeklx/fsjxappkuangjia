package com.example.slbappcomm.agentwebview.activity;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.example.slbappcomm.R;
import com.example.slbappcomm.base.BaseActWebActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

public class SmartRefreshWebActDemo extends BaseActWebActivity {
    private SmartRefreshLayout smarkLayout;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_refresh_webview;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        smarkLayout = findViewById(R.id.smarkLayout);
        smarkLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                mAgentWeb.getUrlLoader().reload();

                smarkLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        smarkLayout.finishRefresh();
                    }
                }, 2000);
            }
        });
        smarkLayout.autoRefresh();
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.ll_base_container);
    }

    @Nullable
    @Override
    public String getUrl() {
        return "https://www.baidu.com";
    }
}
