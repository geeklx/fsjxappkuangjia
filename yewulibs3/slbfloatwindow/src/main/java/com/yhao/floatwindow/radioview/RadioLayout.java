package com.yhao.floatwindow.radioview;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import java.util.ArrayList;

/**
 * 类似RadioGrouop效果...和RadioSelector的实现类合用...
 *
 * @author Reisen at 2017-11-29
 */

public class RadioLayout extends LinearLayout {
    private int mChekcdId = View.NO_ID;
    private OnCheckedChangeListener mListener;
    private ArrayList<RadioSelector> mList = new ArrayList<>();

    public RadioLayout(Context context) {
        this(context, null);
    }

    public RadioLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RadioLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public RadioLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void cleanRadioChecked() {
        checkedRadioSelector(View.NO_ID);
    }

    public void checkedRadioSelector(int resId) {
        if (mChekcdId == resId) {
            mChekcdId = View.NO_ID;
        } else {
            mChekcdId = resId;
        }
        //取消其他选中效果
        for (RadioSelector view : mList) {
//            if (view.getId() == resId) {
//                continue;
//            }
            view.setChecked(view.getId() == mChekcdId);
        }
        if (mListener != null) {
            mListener.checkedChange(mChekcdId);
        }
    }

    public int getRadioSelectCount() {
        int j = 0;
        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i) instanceof RadioSelector) {
                j++;
            }
        }
        return j;
    }

    public RadioSelector getRadioSelectorAt(int index) {
        return mList.get(index);
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        mListener = listener;
    }

    public interface OnCheckedChangeListener {
        void checkedChange(int resId);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (child instanceof RadioSelector) {
            RadioSelector view = (RadioSelector) child;
            mList.add(view);
            if (view.isChecked()) {
                mChekcdId = view.getId();
                view.setChecked(true);
            }
        }
        super.addView(child, index, params);
    }
}
