package com.yhao.floatwindow.radioview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.example.fixedfloatwindow.R;


/**
 * 因为普通控件没有checked状态,<br/>
 * 所以子控件中用selected代替checked
 *
 * @author Reisen at 2017-11-29
 */

public class RadioView extends LinearLayout implements RadioSelector {

    boolean isChecked;

    public RadioView(Context context) {
        this(context, null);
    }

    public RadioView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RadioView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(21)
    public RadioView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        boolean clickable = true;
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RadioView);
            clickable = array.getBoolean(R.styleable.RadioView_radioClickable, true);

            array.recycle();
        }
        setOnClickListener(this);
        setClickable(clickable);
    }


    @Override
    public void onClick(View v) {
        ViewParent parent = getParent();
        if (!(parent instanceof RadioLayout)) {
            return;
        }
        RadioLayout layout = (RadioLayout) parent;
        layout.checkedRadioSelector(getId());
    }

    /**
     * @return checked change ?
     */
    public boolean setChecked(boolean checked) {
        return check(checked);
    }

    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        check(selected);
    }

    private boolean check(boolean check) {
        if (this.isChecked == check) {
            return false;
        }
        this.isChecked = check;
        setSelected(check);
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setSelected(check);
        }
        return true;
    }

}
