package com.exam.student.applinks.teachingcoueses.classrecord;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.exam.student.applinks.R;

public class ClassRecordAdapter extends BaseQuickAdapter<ClassRecordDataBindingBean, BaseViewHolder> {

    public ClassRecordAdapter() {
        super(R.layout.recycleview_class_record_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, ClassRecordDataBindingBean item) {
        TextView tvClassTime = helper.itemView.findViewById(R.id.tv_class_time);//上课时间
        TextView tvClassRecordName = helper.itemView.findViewById(R.id.tv_class_record_name);//课程名称
        TextView tvClassName = helper.itemView.findViewById(R.id.tv_class_name);//上课班级名称
        tvClassTime.setText(item.getCreateTime());
        tvClassRecordName.setText(item.getClassName());
        tvClassName.setText(item.getName());
    }
}
