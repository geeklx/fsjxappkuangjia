package com.exam.student.applinks.recyclertab;

public class RecyclerTabBean {
    private boolean isSelected;
    private String id;
    private String text;
    private String name;

    public RecyclerTabBean() {

    }

    public RecyclerTabBean(String id, String text, String name) {
        this.id = id;
        this.text = text;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
