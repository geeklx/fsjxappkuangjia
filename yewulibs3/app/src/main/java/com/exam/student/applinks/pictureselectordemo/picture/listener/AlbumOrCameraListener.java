package com.exam.student.applinks.pictureselectordemo.picture.listener;

/**
 * 选择照片
 *
 * @author wangchunxiao
 * @date 2018/3/7
 */
public interface AlbumOrCameraListener {
    void selectAlbum();//相机

    void selectCamera();//拍照
}
