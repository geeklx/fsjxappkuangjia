package com.exam.student.applinks.tablayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by zxn on 2019/1/23.
 */
public class NewsPagerAdapter1 extends FragmentPagerAdapter {
    private List<Fragment> fragmentList;
    private List<String> list_Title; //tab名的列表

    public NewsPagerAdapter1(List<Fragment> fragmentList, FragmentManager fm, List<String> list_Title) {
        super(fm);
        this.fragmentList = fragmentList;
        this.list_Title = list_Title;
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }
    //此方法用来显示tab上的名字
    @Override
    public CharSequence getPageTitle(int position) {
        return list_Title.get(position % list_Title.size());
    }
}
