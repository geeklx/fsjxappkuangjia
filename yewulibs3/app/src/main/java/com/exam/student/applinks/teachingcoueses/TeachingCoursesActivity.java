package com.exam.student.applinks.teachingcoueses;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class TeachingCoursesActivity extends BaseActivity implements TeachingCoursesViews {
    TeachingCoursesPresenter teachingCoursesPresenter;
    private RecyclerView rvTree;
    private TeachingCoursesAdapter teachingCoursesAdapter;
    private String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        token="Bearer 26459844-34ba-424a-ad94-e6aa2ccf2046";
        setContentView(R.layout.activity_teaching_courses);
        teachingCoursesPresenter = new TeachingCoursesPresenter();
        teachingCoursesPresenter.onCreate(this);
        teachingCoursesPresenter.queryTree(token);
        rvTree=findViewById(R.id.rv_tree);
        onclick();

    }
   private List<CoursesDataBindingBean> mData;

    private void donetwork(List<TeachingCoursesBean.RowsBean> treeBeans) {
        mData = new ArrayList<>();
        if (treeBeans.size() == 0 || treeBeans == null) {
           return;
        } else {
            for (int i = 0; i < treeBeans.size(); i++) {
                mData.add(new CoursesDataBindingBean(treeBeans.get(i).getCourseName(), treeBeans.get(i).getCreateTime(), treeBeans.get(i).getChapterName(), treeBeans.get(i).getSourseNum(), treeBeans.get(i).getTestingNum()));
            }
            teachingCoursesAdapter.setNewData(mData);
        }
    }

    private void onclick() {
        rvTree.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
        teachingCoursesAdapter = new TeachingCoursesAdapter();
        rvTree.setAdapter(teachingCoursesAdapter);
        teachingCoursesAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ToastUtils.showLong(position);
            }
        });
    }

    @Override
    public void onCehuaSuccess(TeachingCoursesBean teachingCoursesBean) {
        donetwork(teachingCoursesBean.getRows());
    }



    @Override
    public void onCehuaNodata(String msg) {
        ToastUtils.showLong(msg);
    }

    @Override
    public void onCehuaFail(String msg) {
        ToastUtils.showLong(msg);
    }


}
