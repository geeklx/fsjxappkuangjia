package com.exam.student.applinks.waterfallflowrecycler.adapter;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.exam.student.applinks.R;
import com.exam.student.applinks.waterfallflowrecycler.bean.OneflowBean;
import com.makeramen.roundedimageview.RoundedImageView;

public class Shoppingadapter extends BaseQuickAdapter<OneflowBean, BaseViewHolder> {

    public Shoppingadapter() {
        super(R.layout.goods_shopping_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, OneflowBean item) {
        TextView tv1 = helper.itemView.findViewById(R.id.tv1);
        ImageView iv1 = helper.itemView.findViewById(R.id.iv1);
        RoundedImageView newCommendimage = (RoundedImageView) helper.itemView.findViewById(R.id.riv_new_commend_image);

        ViewGroup.LayoutParams para = newCommendimage.getLayoutParams();
        para.width = ViewGroup.LayoutParams.MATCH_PARENT;
        para.height = (int) (400 + Math.random() * 400);
        newCommendimage.setLayoutParams(para);
//        tv1.setText(item.getTab_name());
//        iv1.setImageResource(item.getTab_icon());
    }
}
