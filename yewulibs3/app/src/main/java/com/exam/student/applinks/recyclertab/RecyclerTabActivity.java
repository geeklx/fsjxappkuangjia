package com.exam.student.applinks.recyclertab;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.exam.student.applinks.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerTabActivity extends AppCompatActivity {
    private RecyclerView rv;
    TextView tvEdit;
    private RecyclerTabAdapter mAdapter;
    private List<RecyclerTabBean> mList;
    private boolean isEditShow = false;//默认为编辑
    private List<String> ids = new ArrayList<>();//存储id用于批量删除

    ImageView imgCheckbox;//全选按钮
    LinearLayout llChckAll;
    TextView tvDel;//删除按钮
    private int d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_tab);
        rv = findViewById(R.id.rv);
        tvEdit = findViewById(R.id.tv_edit);
        llChckAll = findViewById(R.id.ll_selectall);
        imgCheckbox = findViewById(R.id.img_check);
        tvDel = findViewById(R.id.tv_del);

        rv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerTabAdapter(R.layout.activity_recycler_tab_item);
        View footView = getFooterView(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //添加数据
                d++;
                mList.add(new RecyclerTabBean("id-add" + d, "text-add" + d, "name-add" + d));
                mAdapter.notifyDataSetChanged();
                Toast.makeText(RecyclerTabActivity.this, "--跳转或者dialog--", Toast.LENGTH_SHORT).show();
            }
        });
        mAdapter.addFooterView(footView);

        rv.setAdapter(mAdapter);

        initData();
        initListener();
    }

    public static List<RecyclerTabBean> getData(int lenth) {
        List<RecyclerTabBean> list = new ArrayList<>();
        for (int i = 0; i < lenth; i++) {
            RecyclerTabBean status = new RecyclerTabBean();
            status.setId(String.valueOf(i));
            status.setText("text" + i);
            status.setName("name" + i);
            list.add(status);
        }
        return list;
    }

    private void initData() {
        mList = new ArrayList<>();
        mList = getData(3);
        mAdapter.setNewData(mList);
        mAdapter.notifyDataSetChanged();

    }

    private void initListener() {
        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEditShow) {
                    //当点击的是编辑时
                    tvEdit.setText("OK");
                    ids.clear();
                    for (RecyclerTabBean bean : mList) {
                        bean.setSelected(false);
                    }


                    llChckAll.setVisibility(View.VISIBLE);
                    mAdapter.setEdit(true);
                } else {//当点击的是ok时
                    tvEdit.setText("编辑");
                    mAdapter.setEdit(false);
                    imgCheckbox.setSelected(false);

                    llChckAll.setVisibility(View.GONE);
                }
                isEditShow = !isEditShow;
                mAdapter.notifyDataSetChanged();
            }
        });
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (isEditShow) {
                    mList.get(position).setSelected(!mList.get(position).isSelected());
                    mAdapter.notifyDataSetChanged();
                    if (mList.get(position).isSelected()) {
                        ids.add(String.valueOf(mList.get(position).getId()));
                    } else {
                        ids.remove(String.valueOf(mList.get(position).getId()));
                    }

                    //判断是否全选
                    boolean isAll = true;
                    for (RecyclerTabBean bean : mList) {
                        if (!bean.isSelected()) {
                            isAll = false;
                        }

                    }
                    imgCheckbox.setSelected(isAll);

                }
                mAdapter.notifyDataSetChanged();
            }
        });

        llChckAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imgCheckbox.isSelected()) {//全选时进入
                    ids.clear();
                    imgCheckbox.setSelected(false);
                    for (RecyclerTabBean bean : mList) {
                        bean.setSelected(false);
                    }
                } else {
                    //非全选时
                    for (RecyclerTabBean bean : mList) {
                        ids.add(String.valueOf(bean.getId()));
                    }
                    imgCheckbox.setSelected(true);
                    for (RecyclerTabBean bean : mList) {
                        bean.setSelected(true);
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        });
        tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(RecyclerTabActivity.this, "--" + ids.toString() + "--", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private View getFooterView(View.OnClickListener listener) {
        View view = getLayoutInflater().inflate(R.layout.activity_recycler_item_footer, (ViewGroup) rv.getParent(), false);
        view.setOnClickListener(listener);
        return view;
    }
}
