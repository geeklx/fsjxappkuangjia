package com.exam.student.applinks.pictureselectordemo.mvp;

import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

/**
 * 描述：所有Activity的基类，任何Activity必须继承它
 * －
 * import android.app.Activity;
 * import android.os.Build;
 * import android.os.Bundle;
 * <p>
 * <p>
 * import android.support.v4.app.FragmentActivity;
 * import android.view.WindowManager;
 * <p>
 * import com.sdzn.tank.com.sdzn.pkt.teacher.hd.app.AppContext;
 * import com.sdzn.tank.com.sdzn.pkt.teacher.hd.ui.activity.ActivityManager;
 * import com.sdzn.tank.com.sdzn.pkt.teacher.hd.utils.ProgressDialogManager;
 * 创建人：zhangchao
 * 创建时间：17/3/20
 */
public abstract class BaseActivity extends AppCompatActivity {

    //    protected Application appContext;
    protected BaseActivity activity;
    protected float density;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
//        appContext = getApplication();
        activity = this;
        density = getResources().getDisplayMetrics().density;


    }

    /**
     * 初始化组件
     */
    protected abstract void initView();

    /**
     * 初始化数据
     */
    protected abstract void initData();

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}