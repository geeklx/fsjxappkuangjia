package com.exam.student.applinks.newwork.api;

import com.exam.student.applinks.teachingcoueses.TeachingCoursesBean;
import com.exam.student.applinks.newwork.bean.HCategoryBean;
import com.exam.student.applinks.teachingcoueses.classrecord.ClassRecordBean;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Api {

    String SERVER_ISERVICE = "https://m.hexiangjiaoyu.com/bcapi/";

    /*课堂教学*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("http://doc.znclass.com/course/api/classRoomStaty/selectClassLibListByKointId")
    Call<ResponseSlbBean1<TeachingCoursesBean>> queryTree(@Header("Authorization") String token, @Body RequestBody body);

    /*上课记录*/
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST("http://doc.znclass.com/course/api/ClassRoomTesting/classRecordList")
    Call<ResponseSlbBean1<ClassRecordBean>> queryclassrecord(@Header("Authorization") String token, @Body RequestBody body);

    // 所有分类的列表
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(SERVER_ISERVICE + "liveApp/enum/liveEnumLists")
    Call<ResponseSlbBean<HCategoryBean>> get_category(@Body RequestBody body);

    // 所有分类的列表2
    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @POST(SERVER_ISERVICE + "liveApp/enum/getSubjectsByGrade")
    Call<ResponseSlbBean<HCategoryBean>> get_category2(@Body RequestBody body);
}
