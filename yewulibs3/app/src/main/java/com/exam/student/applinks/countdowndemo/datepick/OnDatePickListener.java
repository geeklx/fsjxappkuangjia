package com.exam.student.applinks.countdowndemo.datepick;


public interface OnDatePickListener {
    void onClick(DatePickWheelDialog dialog);
}
