package com.exam.student.applinks.tablayout;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.dynamirecyclerview.fragment.F2;
import com.exam.student.applinks.dynamirecyclerview.fragment.F3;
import com.kekstudio.dachshundtablayout.DachshundTabLayout;
import com.kekstudio.dachshundtablayout.HelperUtils;
import com.kekstudio.dachshundtablayout.indicators.LineFadeIndicator;

import java.util.ArrayList;
import java.util.List;

public class TabLayoutActivity extends BaseActivity {
    private ViewPager viewPager;
    private DachshundTabLayout tabLayout;

    private ArrayList<Fragment> fragments = new ArrayList<>();
    private F2 fragment1;
    private F3 fragment2;
    private List<String> list_title; //tab名称列表
    NewsPagerAdapter1 fAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);
        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_layout);
        LineFadeIndicator lineFadeIndicator = new LineFadeIndicator(tabLayout);
        tabLayout.setAnimatedIndicator(lineFadeIndicator);
        lineFadeIndicator.setSelectedTabIndicatorHeight(HelperUtils.dpToPx(2));
        lineFadeIndicator.setEdgeRadius(0);

        showinit();
    }

    private void showinit() {
        fragment1 = new F2();
        fragment2 = new F3();

        fragments = new ArrayList<>();
        fragments.add(fragment1);
        fragments.add(fragment2);

        list_title = new ArrayList<>();
        list_title.add("关注");
        list_title.add("推荐");

        tabLayout.addTab(tabLayout.newTab().setText(list_title.get(0)));
        tabLayout.addTab(tabLayout.newTab().setText(list_title.get(1)));

        fAdapter = new NewsPagerAdapter1(fragments, getSupportFragmentManager(), list_title);
        viewPager.setAdapter(fAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
