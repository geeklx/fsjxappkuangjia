package com.exam.student.applinks;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.exam.student.applinks.base.ActivityManager;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.base.Event;
import com.exam.student.applinks.diagramdemo.DiagramActivity;
import com.exam.student.applinks.graffitidemo.OnScreenCaptureEvent;
import com.exam.student.applinks.recyclerDemo.RecyclerActivity;
import com.exam.student.applinks.recyclertab.RecyclerTabActivity;
import com.exam.student.applinks.timeselector.TimeSelectorActivity;
import com.exam.student.applinks.updateappdemo.UpdateAppActivity;
import com.exam.student.applinks.videoplaydemo.VideoPlayActivity;
import com.exam.student.applinks.webviewdemo.shouyeWebviewActivity;
import com.exam.student.applinks.xpopupdemo.PopupActivity;
import com.exam.student.applinks.zxingdemo.SaomaActDemo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

import static com.exam.student.applinks.basedemo.BaseActDemo.URL_KEY;

public class MainActivity extends BaseActivity {
    private Button btn_sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);
//        SPUtils.getInstance().put("url", "www.baidu.com");
//        btn_sp = findViewById(R.id.btn_sp);
//        btn_sp.setText(SPUtils.getInstance().getString("url"));

        /*自定义拨号*/
        findViewById(R.id.tv_CustomNumer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.CustomNumericKeyboardActivity"));
            }
        });
        /*树结构列表*/
        findViewById(R.id.tv_treelist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.RecyclerViewActivity"));
            }
        });
        /*RecyclerView多选*/
        findViewById(R.id.tv_rv_multiple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.MultipleRecyclerViewActivity"));
            }

        });
        /*RecyclerView单选*/
        findViewById(R.id.tv_rvsingle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SingleRecyclerViewActivity"));
            }
        });
        /*瀑布流*/
        findViewById(R.id.tv_waterfallflowrecycler).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.WaterfallFlowActivity"));
            }
        });
        findViewById(R.id.btn_class_record).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ClassRecordActivity"));
            }
        });

        findViewById(R.id.btn_tree).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.TeachingCoursesActivity"));
            }
        });

        findViewById(R.id.btn_sp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SpActivity"));
            }
        });
        findViewById(R.id.btn_tablayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.TabLayoutActivity"));
            }
        });

        findViewById(R.id.btn_shoppingcart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShoppingCartActivity"));
            }
        });
        findViewById(R.id.btn_expandableview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.expandableact");
                startActivity(intent);
            }
        });
        /*真实网络状态空白页处理*/
        findViewById(R.id.btn_basicemptyview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.BasicExhibitionAct");
                startActivity(intent);
            }
        });
//        /*界面网络状态*/
//        findViewById(R.id.btn_emptyview).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.EmptyViewMainActivity");
//                startActivity(intent);
//            }
//        });

        /*自定义basetitle带Tablayout*/
        findViewById(R.id.btn_baseacttablyout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.BaseActDemoYewutablayou");
                startActivity(intent);
            }
        });

        /*自定义basetitle引用js*/
        findViewById(R.id.btn_baseactjs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.BaseActDemoYewuJS");
                intent.putExtra(URL_KEY, "file:///android_asset/js_interaction/hello.html");
                startActivity(intent);
            }
        });
        /*自定义basetitle基础用法*/
        findViewById(R.id.btn_baseactjichu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.BaseActDemoYewu");
//                intent.putExtra(URL_KEY, "https://www.baidu.com/");
//                intent.putExtra(URL_KEY, "file:///android_asset/js_interaction/hello1.html");
                intent.putExtra(URL_KEY, "https://www.baidu.com/");
                startActivity(intent);
            }
        });

        /*弹窗*/
        findViewById(R.id.btn_popup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PopupActivity.class);
                startActivity(intent);
            }
        });
        /*时间选择器*/
        findViewById(R.id.btn_time_selector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TimeSelectorActivity.class);
                startActivity(intent);
            }
        });
        /*E-chat曲线图*/
        findViewById(R.id.btn_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DiagramActivity.class);
                startActivity(intent);
            }
        });
        /*视频播放器*/
        findViewById(R.id.btn_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, VideoPlayActivity.class);
                startActivity(intent);
//                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.VideoPlayActivity"));
            }
        });

        /*图片查看*/
        findViewById(R.id.btn_pictureviewer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.PictureviewerActivity"));
            }
        });
        //自定义首页
        findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShouyeActivity"));
            }
        });
        /*权限demo*/
        findViewById(R.id.btn_easypermissions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.CaptureActivity"));
            }
        });
        /*极光分享*/
        findViewById(R.id.btn_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.ShareIndexActivity");
                startActivity(startIntent);
            }
        });
        /*扫码*/
        findViewById(R.id.btn_saoma).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SaomaActDemo.class);
                startActivity(intent);
            }
        });
        /*webView*/
        findViewById(R.id.btn_webview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, shouyeWebviewActivity.class);
                startActivity(intent);
            }
        });
        /*进入图片选择器*/
        findViewById(R.id.btn_pic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.MainPictureActivity"));
            }
        });

        /*RecyclerView局部刷新*/
        findViewById(R.id.btn_recycler).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intent);
            }
        });
        /*版本更新*/
        findViewById(R.id.btn_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UpdateAppActivity.class);
                startActivity(intent);
            }
        });
        /*recycler的标签*/
        findViewById(R.id.btn_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RecyclerTabActivity.class);
                startActivity(intent);
            }
        });
        /*Hios协议*/
        findViewById(R.id.btn_hios).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.webview.DemoWebviewMainActivity");//ShoppingCartActivity
                startActivity(intent);
            }
        });
    }

    private static final int REQUECT_CODE_SDCARD = 1000;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(Event event) {
        if (event instanceof OnScreenCaptureEvent) {
            requestPermissiontest();
        }
    }

    public void requestPermissiontest() {
        String[] perms = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // 已经申请过权限，做想做的事s
            if (ActivityManager.getForegroundActivity() != null) {
                if (App.mMediaProjection == null) {
                    ActivityManager.getForegroundActivity().startActivityForResult(
                            mMediaProjectionManager.createScreenCaptureIntent(),
                            REQUEST_MEDIA_PROJECTION_CAPTURE);
                } else {
                    ((BaseActivity) ActivityManager.getForegroundActivity()).getScreen();
                }
            }
        } else {
            // 没有申请过权限，现在去申请
            /**
             *@param host Context对象
             *@param rationale  权限弹窗上的提示语。
             *@param requestCode 请求权限的唯一标识码
             *@param perms 一系列权限
             */
            EasyPermissions.requestPermissions(this, "请设置权限", REQUECT_CODE_SDCARD, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions, @NonNull int[] grantResults) {
//        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    public List<GradeJson> getGrade() {
        try {
            InputStream open = this.getResources().getAssets().open("grade.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                List<GradeJson> lists = new Gson().fromJson(json, new TypeToken<List<GradeJson>>() {
                }.getType());
                Log.e("aaatest", String.valueOf(lists));
                return new Gson().fromJson(json, new TypeToken<List<GradeJson>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
