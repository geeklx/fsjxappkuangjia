package com.exam.student.applinks.recyclertab;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.exam.student.applinks.R;


public class RecyclerTabAdapter extends BaseQuickAdapter<RecyclerTabBean, BaseViewHolder> {

    protected boolean isEdit = false;

    public RecyclerTabAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, RecyclerTabBean item) {

        helper.setText(R.id.text, item.getText());
        helper.setText(R.id.name, item.getName());
        if (isEdit) {
            helper.setVisible(R.id.img_check, true);
        } else {
            helper.setVisible(R.id.img_check, false);
        }
        ImageView imageView = helper.getView(R.id.img_check);
        imageView.setSelected(item.isSelected());
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
        notifyDataSetChanged();
    }

    public boolean getEdit() {
        return isEdit;
    }
}
