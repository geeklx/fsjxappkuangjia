package com.exam.student.applinks.videoplaydemo.gsy;


import android.app.Activity;
import android.content.Intent;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.exam.student.applinks.R;
import com.exam.student.applinks.videoplaydemo.ListVideo2Activity;
import com.exam.student.applinks.videoplaydemo.WindowActivity;

public class JumpVideoPlayUtils {

    public final static String IMG_TRANSITION = "IMG_TRANSITION";
    public final static String TRANSITION = "TRANSITION";

    public static void goToPlayEmptyControlActivity(Activity activity, View view) {
        Intent intent = new Intent(activity, SimplePlayer.class);//AppUtils.getAppPackageName() + "hs.act.slbapp.SimplePlayer"
        intent.putExtra(TRANSITION, true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Pair pair = new Pair<>(view, IMG_TRANSITION);
            ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity, pair);
            ActivityCompat.startActivity(activity, intent, activityOptions.toBundle());
        } else {
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
    }

    /**
     * 跳转到详情播放
     *
     * @param activity
     */
    public static void goToScrollWindow(Activity activity) {
        Intent intent = new Intent(activity, WindowActivity.class);
        activity.startActivity(intent);
    }


    /**
     * 跳转到视频列表2
     *
     * @param activity
     */
    public static void goToVideoPlayer2(Activity activity) {
        Intent intent = new Intent(activity, ListVideo2Activity.class);
        ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity);
        ActivityCompat.startActivity(activity, intent, activityOptions.toBundle());
    }

}
