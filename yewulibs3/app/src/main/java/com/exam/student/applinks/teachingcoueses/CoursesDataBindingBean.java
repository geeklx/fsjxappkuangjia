package com.exam.student.applinks.teachingcoueses;

import java.io.Serializable;

public class CoursesDataBindingBean implements Serializable {
    private String courseName;//课程名称
    private String createTime;//创建时间
    private String chapterName;//章节名称
    private String sourseNum;//资料
    private String testingNum;//检测


    public CoursesDataBindingBean(String courseName, String createTime, String chapterName, String sourseNum, String testingNum) {
        this.courseName = courseName;
        this.createTime = createTime;
        this.chapterName = chapterName;
        this.sourseNum = sourseNum;
        this.testingNum = testingNum;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getSourseNum() {
        return sourseNum;
    }

    public void setSourseNum(String sourseNum) {
        this.sourseNum = sourseNum;
    }

    public String getTestingNum() {
        return testingNum;
    }

    public void setTestingNum(String testingNum) {
        this.testingNum = testingNum;
    }
}
