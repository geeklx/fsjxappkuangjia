package com.exam.student.applinks.recyclerDemo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.R;
import com.exam.student.applinks.recyclerDemo.adapter.MyDiffAdapter;
import com.exam.student.applinks.recyclerDemo.modle.TextModel;
import com.exam.student.applinks.recyclerDemo.modle.UpdateAvatarEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends BaseActivity {
    private List<TextModel> mTextModels;
    private RecyclerView text_rv;
    private TextView ref_rv;
    //   private MyAdapter myAdapter;
    private MyDiffAdapter myDiffAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        EventBus.getDefault().register(this);
        text_rv = findViewById(R.id.text_rv);
        ref_rv = findViewById(R.id.ref_rv);
        initData();
        initRv();
        setControl();
    }

    private void setControl() {
        ref_rv.setOnClickListener(v -> {
            Intent intent = new Intent(this, RecyclerActivity2.class);
            startActivity(intent);
//            change1();
        });
    }

    private void initRv() {
        text_rv.setLayoutManager(new LinearLayoutManager(this));
//        myAdapter = new MyAdapter(MainActivity.this);
//        myAdapter.setData(mTextModels);
        myDiffAdapter = new MyDiffAdapter(RecyclerActivity.this);
        myDiffAdapter.submitList(mTextModels);
        //这个是使用diffutils
//        text_rv.setAdapter(myAdapter);
        //这个是使用AsyncListDiff
        text_rv.setAdapter(myDiffAdapter);
    }

    private void initData() {
        mTextModels = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TextModel textModel = new TextModel("aa" + i, "bb" + i);
            mTextModels.add(textModel);
        }

    }

    public void change1() {
        mTextModels = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            TextModel textModel = new TextModel("aa" + i, "bb" + i % 5);
            mTextModels.add(textModel);
        }
        myDiffAdapter.submitList(mTextModels);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpPhotoUIEvent(UpdateAvatarEvent upPhotoUIEvent) {
        if (upPhotoUIEvent != null) {
            change1();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
