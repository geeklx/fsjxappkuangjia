package com.exam.student.applinks.teachingcoueses.classrecord;

import com.alibaba.fastjson.JSONObject;
import com.exam.student.applinks.newwork.api.Api;
import com.haier.cellarette.libmvp.mvp.Presenter;
import com.haier.cellarette.libretrofit.common.BanbenUtils;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;
import com.haier.cellarette.libretrofit.common.RetrofitNetNew;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 描述：
 * -
 * 创建人：baoshengxiang
 * 创建时间：2017/6/26
 */
public class ClassRecordPresenter extends Presenter<ClassRecordViews> {

    public void queryClassRecord(String token) {
        JSONObject requestData = new JSONObject();
        requestData.put("chapterId", 2002623);
        requestData.put("classId", 112);
        requestData.put("page", 1);
        requestData.put("limit", 10);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), requestData.toString());


        RetrofitNetNew.build(Api.class, getIdentifier())
                .queryclassrecord(token, requestBody)
                .enqueue(new Callback<ResponseSlbBean1<ClassRecordBean>>() {
                    @Override
                    public void onResponse(Call<ResponseSlbBean1<ClassRecordBean>> call, Response<ResponseSlbBean1<ClassRecordBean>> response) {
                        if (!hasView()) {
                            return;
                        }
                        if (response.body() == null) {
                            return;
                        }
                        if (response.body().getCode() != 0) {
                            getView().onCehuaNodata(response.body().getMessage());
                            return;
                        }
                        getView().onCehuaSuccess(response.body().getResult());
                        call.cancel();
                    }

                    @Override
                    public void onFailure(Call<ResponseSlbBean1<ClassRecordBean>> call, Throwable t) {
                        if (!hasView()) {
                            return;
                        }
                        String string = BanbenUtils.getInstance().error_tips;
                        getView().onCehuaFail(string);
                        t.printStackTrace();
                        call.cancel();
                    }
                });
    }
}
