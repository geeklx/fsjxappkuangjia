package com.exam.student.applinks.androidtreeview;

import java.util.List;

public class DataChildBean {
    /**
     * data : {"id":2007055,"name":"落日","parentId":2007054,"parentName":"第一单元","seq":0,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0}
     * children : []
     * parent : {}
     * leaf : true
     */

    private DataInfoBean data;
//    private ParentBeanX parent;
    private boolean leaf;
    private List<DataChildBean> children;

    public DataInfoBean getData() {
        return data;
    }

    public void setData(DataInfoBean data) {
        this.data = data;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public List<DataChildBean> getChildren() {
        return children;
    }

    public void setChildren(List<DataChildBean> children) {
        this.children = children;
    }
}
