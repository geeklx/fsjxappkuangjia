package com.exam.student.applinks.recyclerDemo.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.exam.student.applinks.R;
import com.exam.student.applinks.recyclerDemo.modle.MyItemCallBack;
import com.exam.student.applinks.recyclerDemo.modle.TextModel;

import java.util.List;

public class MyDiffAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private AsyncListDiffer<TextModel> mTextDiffl;
    private DiffUtil.ItemCallback<TextModel> diffCallback = new MyItemCallBack();

    public MyDiffAdapter(Context mContext) {
        this.mContext = mContext;
        mTextDiffl = new AsyncListDiffer<>(this, diffCallback);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_view, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.e("ref", "position: " + position);
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        TextModel textModel = getItem(position);
        myViewHolder.tv.setText(textModel.getTextTitle() + "." + textModel.getTextContent());
    }

    @Override
    public int getItemCount() {
        return mTextDiffl.getCurrentList().size();
    }

    public void submitList(List<TextModel> data) {
        Log.e("ljc ", data.toString());
        mTextDiffl.submitList(data);
    }

    public TextModel getItem(int position) {
        return mTextDiffl.getCurrentList().get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv;

        MyViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.item_tv);
        }
    }
}
