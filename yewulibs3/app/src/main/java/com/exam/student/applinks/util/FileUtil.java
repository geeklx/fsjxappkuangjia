package com.exam.student.applinks.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.util.Utils;
import com.exam.student.applinks.App;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.R.attr.path;

/**
 * @author liujun 2012-11-22 下午05:38:17
 */
public final class FileUtil {
    public static Boolean existsSdcard() {
        return Boolean.valueOf(Environment.getExternalStorageState().equals("mounted"));
    }

    /**
     * 根据系统时间、前缀、后缀产生一个文件
     */
    public static File createFile(File folder, String prefix, String suffix) {
        if (!folder.exists() || !folder.isDirectory()) folder.mkdirs();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.CHINA);
        String filename = prefix + dateFormat.format(new Date(System.currentTimeMillis())) + suffix;
        return new File(folder, filename);
    }

    /**
     * 创建文件夹
     *
     * @param tempNewCameraImage
     */
    public static void makeDir(String tempNewCameraImage) {
        File directory = new File(tempNewCameraImage)
                .getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            try {
                throw new IOException("Path to file could not be created.");
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }

    /**
     * 通过Uri获得文件路径
     *
     * @param uri
     * @param activity
     * @return
     */
    public static String getUriPath(Uri uri, Activity activity) {

        String path = null;
        Cursor cursor = null;
        try {
            path = uri.toString();
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = activity.managedQuery(uri, proj, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                // �?��根据索引值获取图片路�?
                path = cursor.getString(column_index);
            }
            path = path.substring(path.indexOf("/sdcard"), path.length());
        } catch (Exception e) {
            Log.e("checkImage", e.getMessage());
        } finally {
            return path;
        }
    }


    /**
     * 获取课件资源存储路径
     *
     * @return
     * @throws Exception
     */
    public static String getResPath() throws Exception {
        String resPath = AndroidUtil.getSDPath() + "/sdzn/teacher/file/";
        File file = new File(resPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return resPath;
    }

    /**
     * 获取语音存储路径
     *
     * @return
     * @throws Exception
     */
    public static String getMscPath() throws Exception {
        return AndroidUtil.getSDPath() + "/sdzn/student/msc/iat.wav";
    }

    /**
     * 获取错误日志存储路径
     *
     * @return
     * @throws Exception
     */
    public static String getLogPath() throws Exception {
        return AndroidUtil.getSDPath() + "/sdzn/teacher/log/";
    }

    public static String getFilePathFromContentUri(Uri selectedVideoUri,
                                                   ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
//      也可用下面的方法拿到cursor
//      Cursor cursor = this.context.managedQuery(selectedVideoUri, filePathColumn, null, null, null);

        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }

    /**
     * 递归删除文件夹和文件夹内的所有文件
     *
     * @param dir
     * @return
     */
    public static boolean delete(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                // 递归删除目录中的子目录
                for (int i = 0; i < children.length; i++) {
                    boolean success = delete(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }

        }
        // 目录此时为空，可以删除
        File to = new File(dir.getAbsolutePath() + System.currentTimeMillis());
        dir.renameTo(to);
        return to.delete();
    }

    /**
     * 将文件读取为字符串
     *
     * @param fullFilename
     * @return
     */
    public static String readFileContentStr(String fullFilename) {
        String readOutStr = null;
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(
                    fullFilename));
            try {
                long len = new File(fullFilename).length();
                if (len > Integer.MAX_VALUE)
                    throw new IOException("File " + fullFilename
                            + " too large, was " + len + " bytes.");
                byte[] bytes = new byte[(int) len];
                dis.readFully(bytes);
                readOutStr = new String(bytes, "UTF-8");
            } finally {
                dis.close();
            }
//			Log.d("readFileContentStr",
//					"Successfully to read out string from file " + fullFilename);
        } catch (IOException e) {
            readOutStr = null;
            Log.d("readFileContentStr", "Fail to read out string from file "
                    + fullFilename);
        }
        return readOutStr;
    }

    /**
     * 拷贝文件
     *
     * @param srcPath
     * @param targetPath
     * @throws IOException
     */
    public static void copyFile(String srcPath, String targetPath)
            throws IOException {
        File srcFile = new File(srcPath);
        File targetFile = new File(targetPath);
        FileInputStream is = new FileInputStream(srcFile);
        FileOutputStream fos = new FileOutputStream(targetFile);
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = is.read(buffer)) != -1) {
            fos.write(buffer, 0, len);
        }
        fos.close();
        is.close();
    }

    /**
     * 拷贝assets目录下的文件
     *
     * @param filename
     * @param targetPath
     * @throws IOException
     */
    public static void copyAssets(Context context, String filename, String targetPath)
            throws IOException {
        File targetFile = new File(targetPath);
        AssetManager am = context.getAssets();
        InputStream is = am.open(filename);
        FileOutputStream fos = new FileOutputStream(targetFile);
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = is.read(buffer)) != -1) {
            fos.write(buffer, 0, len);
        }
        fos.close();
        is.close();
    }


    /**
     * 保存图片
     *
     * @param bitmap
     * @param picName
     * @return
     */
    public String saveBitmap(Context context, Bitmap bitmap, String picName) throws Exception {
        String saveFilePath = "";
        String filePath = "";
        String SDPATH = "";
        if (!TextUtils.isEmpty(picName) && bitmap != null) {
            filePath = getResPath() + "store/";
            File saveFile = new File(filePath, picName);
            saveFilePath = filePath + picName;
            if (saveFile.exists()) {
                saveFile.delete();
            }
            try {
                FileOutputStream out = new FileOutputStream(saveFile);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                saveFilePath = "";
            } catch (IOException e) {
                e.printStackTrace();
                saveFilePath = "";
            }
            Uri localUri = Uri.fromFile(saveFile);
            Intent localIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, localUri);
            context.sendBroadcast(localIntent);
        }
        return saveFilePath;
    }

    /**
     * 返回一个时间串
     *
     * @return Author: wangchunxiao 2016-3-9
     */
    public static String fileName() {
        String str = "";
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int date = c.get(Calendar.DATE);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        if (month < 10)
            str = String.valueOf(year) + "0" + String.valueOf(month);
        else {
            str = String.valueOf(year) + String.valueOf(month);
        }
        if (date < 10)
            str = str + "0" + String.valueOf(date);
        else {
            str = str + String.valueOf(date);
        }
        if (hour < 10)
            str = str + "0" + String.valueOf(hour);
        else {
            str = str + String.valueOf(hour);
        }
        if (minute < 10)
            str = str + "0" + String.valueOf(minute);
        else {
            str = str + String.valueOf(minute);
        }
        if (second < 10)
            str = str + "0" + String.valueOf(second);
        else {
            str = str + String.valueOf(second);
        }
        return str;
    }

    private static String pictureName() throws Exception {
        String picPath = getResPath() + "pic/sdzn_student_";
        File file = new File(picPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return picPath + fileName() + ".jpg";
    }

    /**
     * 存储图片
     *
     * @param bitmap
     * @return Author: wangchunxiao 2016-3-14
     */
    public static String savePicture(Bitmap bitmap) {
        String picturePath = null;
        try {
            picturePath = pictureName();
            File file = new File(picturePath);
            if (file.exists()) {
                file.delete();
            }

            file.createNewFile();
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(file));

            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return picturePath;
    }


    public static String saveImageToGallery(Context context, Bitmap bmp, String fileName) {
        // 首先保存图片
        File appDir = new File(Utils.getApp().getExternalFilesDir(null), "tank_student");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        if (TextUtils.isEmpty(fileName)) {
            fileName = System.currentTimeMillis() + ".jpg";
        }
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        // 其次把文件插入到系统图库
        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    file.getAbsolutePath(), fileName, null);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        // 最后通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)));
        return file.getAbsolutePath();
    }

    public static String saveImageToGallery(Context context, Bitmap bmp) {
        return saveImageToGallery(context, bmp, null);
    }

    public static String getAppFilesDir() {
//        return AppLike.mContext.getFilesDir().getPath();
        return App.mContext.getExternalFilesDir("").getPath();
    }
}
