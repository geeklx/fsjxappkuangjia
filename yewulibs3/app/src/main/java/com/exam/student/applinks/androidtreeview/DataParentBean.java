package com.exam.student.applinks.androidtreeview;

import java.util.List;

public class DataParentBean {
        /**
         * data : {"id":2007054,"name":"第一单元","parentId":0,"parentName":"","seq":0,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0}
         * children : [{"data":{"id":2007055,"name":"落日","parentId":2007054,"parentName":"第一单元","seq":0,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007056,"name":"奥斯维辛没有什么新闻","parentId":2007054,"parentName":"第一单元","seq":1,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007057,"name":"*唐山大地震（节选）","parentId":2007054,"parentName":"第一单元","seq":2,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007058,"name":"字音字形","parentId":2007054,"parentName":"第一单元","seq":3,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007059,"name":"正确使用词语","parentId":2007054,"parentName":"第一单元","seq":4,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007060,"name":"标点符号","parentId":2007054,"parentName":"第一单元","seq":5,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007061,"name":"语言表达应用","parentId":2007054,"parentName":"第一单元","seq":6,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[{"data":{"id":2007062,"name":"选用、仿用、变换句式","parentId":2007061,"parentName":"语言表达应用","seq":0,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007063,"name":"扩展语句","parentId":2007061,"parentName":"语言表达应用","seq":1,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007064,"name":"压缩语段","parentId":2007061,"parentName":"语言表达应用","seq":2,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007065,"name":"修辞手法","parentId":2007061,"parentName":"语言表达应用","seq":3,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007066,"name":"语言表达简明、连贯、得体","parentId":2007061,"parentName":"语言表达应用","seq":4,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true},{"data":{"id":2007067,"name":"图文转换","parentId":2007061,"parentName":"语言表达应用","seq":5,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true}],"parent":{},"leaf":false},{"data":{"id":2007068,"name":"新闻阅读","parentId":2007054,"parentName":"第一单元","seq":7,"bookVolumeId":699,"bookVolumeName":"必修一","bookVersionId":10257,"bookVersionName":"语文版","subjectId":3,"subjectName":"语文","levelId":3,"levelName":"高中","nodeIdPath":"","nodeNamePath":"","isDelete":0},"children":[],"parent":{},"leaf":true}]
         * parent : {}
         * leaf : false
         */

        private DataInfoBean data;
//        private ParentBean parent;
        private boolean leaf;
        private List<DataChildBean> children;

        public DataInfoBean getData() {
            return data;
        }

        public void setData(DataInfoBean data) {
            this.data = data;
        }


        public boolean isLeaf() {
            return leaf;
        }

        public void setLeaf(boolean leaf) {
            this.leaf = leaf;
        }

        public List<DataChildBean> getChildren() {
            return children;
        }

        public void setChildren(List<DataChildBean> children) {
            this.children = children;
        }






}
