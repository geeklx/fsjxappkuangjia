package com.exam.student.applinks.basedemo;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.timeselector.CalenderClearEditTexts;
import com.exam.student.applinks.timeselector.ClearableEditText;
import com.exam.student.applinks.util.StringUtils;
import com.exam.student.applinks.widgits.XRecyclerView;
import com.example.baselibrary.emptyview.EmptyViewNew1;
import com.example.baselibrary.emptyview.networkview.NetState;
import com.example.baselibrary.emptyview.networkview.NetconListener2;
import com.tubb.calendarselector.CalenderDialog;
import com.tubb.calendarselector.OnCalenderSelectListener;

import java.util.Calendar;
import java.util.Objects;

public abstract class BaseActNowebView extends AppCompatActivity implements NetconListener2 {
    public TextView tvBack;//返回
    public TextView tvTitleName;//标题
    public TextView tvDownTitle;//下拉选择
    public XRecyclerView recyclerViewTitle;//滑动选择
    public TextView tvTijiaoTitle;//提交按钮
    public TextView tvSousuoTitle;//搜索按钮
    public TextView tvShijianTitle;//时间按钮
    public TextView tvMyTitle;//个人中心按钮
    protected EmptyViewNew1 emptyview1;//网络监听
    public CalenderClearEditTexts tvZankaiShijian;//时间选择器Edtext
    private long mCurrentMs = System.currentTimeMillis();
    protected NetState netState;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        setup(savedInstanceState);
        findiview();
        //网络监听
        netState = new NetState();
        netState.setNetStateListener(this, this);
        onclickview();
        donetwork();
    }

    /*具体业务内容*/
    protected void donetwork() {

    }

    /*加载布局*/
    protected abstract int getLayoutId();


    /**/
    protected void setup(@Nullable Bundle savedInstanceState) {
        tvBack = (TextView) findViewById(R.id.tv_back);
        tvTitleName = (TextView) findViewById(R.id.tv_title_name);
        tvDownTitle = (TextView) findViewById(R.id.tv_down_title);
        recyclerViewTitle = (XRecyclerView) findViewById(R.id.recycler_view_title);
        tvTijiaoTitle = (TextView) findViewById(R.id.tv_tijiao_title);
        tvSousuoTitle = (TextView) findViewById(R.id.tv_sousuo_title);
        tvShijianTitle = (TextView) findViewById(R.id.tv_shijian_title);
        tvMyTitle = (TextView) findViewById(R.id.tv_my_title);
        tvZankaiShijian = findViewById(R.id.tv_zankai_shijian);
        emptyview1 = findViewById(R.id.emptyview2_order);
        tvZankaiShijian.setClearTextListener(new ClearableEditText.ClearTextListener() {
            @Override
            public void onTextClear() {
                tvShijianTitle.setVisibility(View.VISIBLE);
                tvZankaiShijian.setVisibility(View.GONE);
            }
        });
        /*返回点击*/
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.TitleBack();
                }
            }
        });
        /*下拉点击事件*/
        tvDownTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.TitleDropdown();
                }
            }
        });
        tvTijiaoTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.Titletijiao();
                }
            }
        });
        tvSousuoTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.Titlesousuo();
                }
            }
        });
        tvShijianTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.Titleshijian();
                }
            }
        });
        tvZankaiShijian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.Titlezankaishijian();
                }
            }
        });
        tvMyTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.Titlegrzx();
                }
            }
        });
    }

    /**
     * @param LayoutStyle 返回判断
     */
    protected void TitleShowHideState(int LayoutStyle) {

        if (LayoutStyle == 1) { /*隐藏下拉标签，和RecyclerView，提交*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvSousuoTitle.setVisibility(View.VISIBLE);
            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 2) { /*隐藏搜索，下拉标签*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvTijiaoTitle.setVisibility(View.VISIBLE);
            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 3) { /*隐藏下拉标签，搜索，提交，时间*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 4) {/*只留下时间和个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 5) {    /*只显示个人中心*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        } else if (LayoutStyle == 6) {   /*课程管理-时间筛选*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvSousuoTitle.setVisibility(View.VISIBLE);
            tvShijianTitle.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            return;
        }else if (LayoutStyle == 7) { /*隐藏下拉标签，搜索，提交，时间*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            tvMyTitle.setVisibility(View.VISIBLE);
            tvDownTitle.setVisibility(View.VISIBLE);
            return;
        }else if (LayoutStyle == 8) { /*返回，rv*/
            tvBack.setVisibility(View.VISIBLE);
            tvTitleName.setVisibility(View.VISIBLE);
            recyclerViewTitle.setVisibility(View.VISIBLE);
            return;
        }
    }

    protected void findiview() {

    }


    protected void onclickview() {

    }
    @Override
    public void net_con_none() {
        ToastUtils.showLong("网络异常，请检查网络连接！");
    }

    @Override
    public void net_con_success() {
    }

    @Override
    public void showNetPopup() {
    }

    /**
     * fragment间通讯bufen
     *
     * @param value 要传递的值
     * @param tag   要通知的fragment的tag
     */
    public void callFragment(Object value, String... tag) {
        FragmentManager fm = Objects.requireNonNull(this).getSupportFragmentManager();
        Fragment fragment;
        for (String item : tag) {
            if (TextUtils.isEmpty(item)) {
                continue;
            }

            fragment = fm.findFragmentByTag(item);
            if (fragment instanceof BaseActFragment) {
                ((BaseActFragment) fragment).call(value);
            }
        }
    }

    private BaseOnClickListener mListener;

    public void setBaseOnClickListener(BaseOnClickListener listener) {
        mListener = listener;
    }

    //
//    //个人中心
//    public void Titlegrzx() {
//    }
//
//    //时间
//    public void Titleshijian() {
//        showCalendarDialog();
////        showCalendarDialog();
//    }
//
//    //展开时间
//    public void Titlezankaishijian() {
////        showCalendarDialog();
//    }
//
//    /*搜索*/
//    public void Titlesousuo() {
//    }
//
//    /*提交*/
//    public void Titletijiao() {
//    }
//
//    /*下拉加载*/
//    public void TitleDropdown() {
//    }

    /*返回*/
    public void TitleBack() {
        finish();
    }


    /*时间选择器-------------------------------------开始*/
    private CalenderDialog calendarDialog;//日历dialog

    public String startTime;//开始时间
    public String endTime;//结束时间

    /*时间选择器*/
    protected void showCalendarDialog() {
        if (calendarDialog == null) {
            calendarDialog = new CalenderDialog(this, new OnCalenderSelectListener() {
                @Override
                public void onCalenderSelect(Calendar startCalendar, Calendar endCalendar) {
                    tvZankaiShijian.setVisibility(View.VISIBLE);
                    tvShijianTitle.setVisibility(View.GONE);
                    tvZankaiShijian.setText(StringUtils.transTime(startCalendar.getTime(), "yyyy-MM-dd") + "  ~  "
                            + StringUtils.transTime(endCalendar.getTime(), "yyyy-MM-dd"));
                    startTime = String.valueOf(startCalendar.getTimeInMillis());
                    endTime = String.valueOf(endCalendar.getTimeInMillis());
                    listener.returnRefresh(startTime, endTime);
                }
            });
        }
        calendarDialog.show();

    }

    //回调时间数据
    private static refreshOnDisplayListener listener;

    public interface refreshOnDisplayListener {
        void returnRefresh(String startTime, String endTime);
    }

    public static void setOnDisplayRefreshListener(refreshOnDisplayListener myListener) {
        listener = myListener;
    }

    /*时间选择器-------------------------------------结束*/

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }
    @Override
    protected void onDestroy() {
        if (netState != null) {
            netState.unregisterReceiver();
        }
        super.onDestroy();
    }
}
