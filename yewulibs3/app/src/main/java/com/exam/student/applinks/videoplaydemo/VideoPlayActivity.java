package com.exam.student.applinks.videoplaydemo;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.R;
import com.exam.student.applinks.videoplaydemo.gsy.JumpVideoPlayUtils;


public class VideoPlayActivity extends BaseActivity {
    TextView tv1, tv2, tv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoplay1);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //多窗体下的悬浮窗
                JumpVideoPlayUtils.goToScrollWindow(VideoPlayActivity.this);
            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //简单的播放
                JumpVideoPlayUtils.goToPlayEmptyControlActivity(VideoPlayActivity.this, tv1);
                //      startActivity(new Intent(this, SimplePlayer.class));
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //简单的播放
                JumpVideoPlayUtils.goToVideoPlayer2(VideoPlayActivity.this);
                //      startActivity(new Intent(this, SimplePlayer.class));
            }
        });

    }
}
