package com.exam.student.applinks.zxingdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.AppUtils;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.R;
import com.example.zxinglibs3.SaomaAct3;

public class SaomaActDemo extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saoma_demo);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn0) {
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SaomaAct"));
        } else if (id == R.id.btn1) {
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SaomaAct2"));
        } else if (id == R.id.btn2) {
            startActivity(new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.SaomaAct3"));
//            startActivity(new Intent(this, SaomaAct3.class));
        }

    }
}
