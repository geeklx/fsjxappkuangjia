package com.exam.student.applinks.androidtreeview;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.androidtreeview.holder.ContentHolder;
import com.exam.student.applinks.androidtreeview.holder.IconTreeItem;
import com.exam.student.applinks.base.BaseActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TreeActivity extends BaseActivity {
    LinearLayout containerView;
    AndroidTreeView tView;
    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tree_view);
        containerView = findViewById(R.id.container);

        List<DataParentBean> list = getDataList();
        ToastUtils.showLong("--" + list.toString());


//        Collections.sort(newPointList);
        TreeNode node = TreeNode.root();
        createTreeNodes(node, list);


         tView = new AndroidTreeView(this, node);
        tView.setDefaultAnimation(true);
        //tView.setUse2dScroll(true);
//        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom, true);


//        containerView.addView(tView.getView(R.style.TreeNodeStyle));
        containerView.addView(tView.getView(R.style.TreeNodeStyleCustom));
        tView.expandAll();

        tView.setDefaultNodeClickListener(nodeClickListener);

        Log.e("eeeeeeDataParentBeanList", "--" );


    }

    public List<DataParentBean> getDataList() {
        try {
            InputStream open = getResources().getAssets().open("data.json");
            byte[] buffer = new byte[open.available()];
            int read = open.read(buffer);
            if (read != 0) {
                String json = new String(buffer, "utf-8");
                return new Gson().fromJson(json, new TypeToken<List<DataParentBean>>() {
                }.getType());
            }
        } catch (Exception e) {
            LogUtils.e(e);
        }
        return new ArrayList<>();
    }


    private void createTreeNodes(TreeNode treeNode, List<DataParentBean> dataParentBeans) {
        for (DataParentBean dataParentBean : dataParentBeans) {
            TreeNode node = initTreeNode(dataParentBean.getData().getName());
            treeNode.addChild(node);
            if (dataParentBean != null && !dataParentBean.getChildren().isEmpty()) {
                createTreeNodes(node, dataParentBean.getChildren(), dataParentBean.getData().getId());
            }
        }
    }

    private void createTreeNodes(TreeNode treeNode, List<DataChildBean> dataChildBeans, long parentId) {
        for (DataChildBean dataChildBean : dataChildBeans) {
            if (dataChildBean.getData().getParentId() == parentId) {
                TreeNode node = initTreeNode(dataChildBean.getData().getName());
                treeNode.addChild(node);
                createTreeNodes(node, dataChildBean.getChildren(), dataChildBean.getData().getId());
            }
        }
    }


    //    每一条数据
    private TreeNode initTreeNode(String name) {
        ContentHolder holder =new ContentHolder(this);
        return new TreeNode(new IconTreeItem("1", name))
                .setViewHolder(holder);  //p 图标样式   p内容展示

    }

    ContentHolder adapter=null;
    private TreeNode.TreeNodeClickListener nodeClickListener = new TreeNode.TreeNodeClickListener() {
        @Override
        public void onClick(TreeNode node, Object value) {

            ToastUtils.showLong("同时触发点击Toast---clicked: " + "----" + ((IconTreeItem) value).text);
            if (adapter!=null){
                adapter.setUnselected();
            }
            adapter = (ContentHolder) node.getViewHolder();
            adapter.setSelected();

        }
    };
}
