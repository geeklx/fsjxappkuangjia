package com.exam.student.applinks.teachingcoueses.classrecord;

import java.util.List;

public class ClassRecordBean {

    /**
     * rows : [{"name":"春","className":"一班","createTime":"2020-12-02 11:57:56"},{"name":"春","className":"一班","createTime":"2020-12-02 14:27:12"},{"name":"春","className":"一班","createTime":"2020-12-02 15:28:07"},{"name":"春","className":"一班","createTime":"2020-12-02 15:50:07"},{"name":"春","className":"一班","createTime":"2020-12-02 16:45:19"},{"name":"春","className":"一班","createTime":"2020-12-04 16:15:43"},{"name":"春","className":"一班","createTime":"2020-12-04 16:16:30"},{"name":"春","className":"一班","createTime":"2020-12-04 17:29:11"},{"name":"春","className":"一班","createTime":"2020-12-08 13:53:15"},{"name":"春","className":"一班","createTime":"2020-12-09 10:40:47"}]
     * total : 10
     */

    private int total;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * name : 春
         * className : 一班
         * createTime : 2020-12-02 11:57:56
         */

        private String name;
        private String className;
        private String createTime;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}
