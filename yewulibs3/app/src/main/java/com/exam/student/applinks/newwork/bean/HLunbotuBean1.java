package com.exam.student.applinks.newwork.bean;

import java.io.Serializable;

public class HLunbotuBean1 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String imgUrl;
    private String linkType;
    private String linkUrl;
    private String liveCourseId;
    private String name;
    private String orders;
    private String remark;

    public HLunbotuBean1() {
    }

    public HLunbotuBean1(String id, String imgUrl, String linkType, String linkUrl, String liveCourseId, String name, String orders, String remark) {
        this.id = id;
        this.imgUrl = imgUrl;
        this.linkType = linkType;
        this.linkUrl = linkUrl;
        this.liveCourseId = liveCourseId;
        this.name = name;
        this.orders = orders;
        this.remark = remark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLiveCourseId() {
        return liveCourseId;
    }

    public void setLiveCourseId(String liveCourseId) {
        this.liveCourseId = liveCourseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
