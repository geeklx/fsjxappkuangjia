package com.exam.student.applinks.androidtreeview.holder;

public class IconTreeItem {
    public String icon;
    public String text;

    public IconTreeItem(String icon, String text) {
        this.icon = icon;
        this.text = text;
    }
}
