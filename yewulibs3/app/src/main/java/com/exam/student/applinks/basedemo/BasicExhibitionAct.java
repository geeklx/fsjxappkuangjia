package com.exam.student.applinks.basedemo;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.basedemo.bean.VersionInfoBean;
import com.exam.student.applinks.basedemo.presenter.CheckverionPresenter;
import com.exam.student.applinks.basedemo.view.CheckverionView;
import com.example.baselibrary.emptyview.EmptyView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

public class BasicExhibitionAct extends BaseActNowebView implements BaseOnClickListener, CheckverionView {
    CheckverionPresenter checkverionPresenter;
    protected SmartRefreshLayout refreshLayout1;
    private TextView tv_contain;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_jichu;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        findview();
        onclick();
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("3", "0");
        refreshLayout1.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshLayout) {
                checkverionPresenter.checkVerion("3", "0");
            }
        });
//        //使上拉加载具有弹性效果
//        refreshLayout1.setEnableAutoLoadmore(false);
//        //禁止越界拖动（1.0.4以上版本）
//        refreshLayout1.setEnableOverScrollDrag(false);
//        //关闭越界回弹功能
//        refreshLayout1.setEnableOverScrollBounce(false);
//        refreshLayout1.setScrollBoundaryDecider(new ScrollBoundaryDecider() {
//            @Override
//            public boolean canRefresh(View content) {
//                //webview滚动到顶部才可以下拉刷新
//                MyLogUtil.e("ssssss",""+mAgentWeb.getWebCreator().getWebView().getScrollY());
//                return mAgentWeb.getWebCreator().getWebView().getScrollY() > 0;
//            }
//
//            @Override
//            public boolean canLoadmore(View content) {
//                return false;
//            }
//        });
        emptyview1.bind(refreshLayout1).setRetryListener(new EmptyView.RetryListener() {
            @Override
            public void retry() {
                // 分布局
                emptyview1.loading();
                checkverionPresenter.checkVerion("3", "0");
            }
        });

    }


    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
        TitleShowHideState(7);
        setBaseOnClickListener(this);
    }

    private void findview() {
        refreshLayout1 = findViewById(R.id.refreshLayout1_order);
        tv_contain = findViewById(R.id.tv_contain);
    }

    private void onclick() {
    }

    //个人中心
    @Override
    public void Titlegrzx() {
        ToastUtils.showLong("点击了个人中心");
    }

    @Override
    public void Titleshijian() {

    }

    @Override
    public void Titlezankaishijian() {

    }

    @Override
    public void Titlesousuo() {

    }


    @Override
    public void Titletijiao() {
        ToastUtils.showLong("点击了提交");
    }

    @Override
    public void TitleDropdown() {

    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        ToastUtils.showLong("OnUpdateVersionSuccess" + versionInfoBean.getProgramName());
        tv_contain.setText("数据更新成功");
        emptyview1.success();
        refreshLayout1.finishRefresh(0);
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        ToastUtils.showLong("OnUpdateVersionNodata" + bean);
        emptyview1.nodata();
        refreshLayout1.finishRefresh(false);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showLong("OnUpdateVersionFail" + msg);
        emptyview1.errorNet();
        refreshLayout1.finishRefresh(false);
    }
}
