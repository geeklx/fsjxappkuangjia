package com.exam.student.applinks.dynamirecyclerview.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.ImageViewTarget;
import com.exam.student.applinks.R;
import com.example.basebanner.bannerview.MZBannerView;
import com.example.basebanner.bannerview.RoundImageView;
import com.example.basebanner.bannerview.holder.MZHolderCreator;
import com.example.basebanner.bannerview.holder.MZViewHolder;
import com.example.basebanner.bannerview.transformer.CustomTransformer;

import java.util.ArrayList;
import java.util.List;


public class F1 extends Fragment {
    private MZBannerView fvBanner;
    private List<String> bannerUrls;
    private String url1 = "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3441245796,6185313&fm=26&gp=0.jpg";
    private String url2 = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597727779948&di=0a97d0861d490309c526503a9ddfa571&imgtype=0&src=http%3A%2F%2Fimg1d.xgo-img.com.cn%2Fpics%2F1545%2F1544789.jpg";
    private String url3 = "https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=297759806,1174507829&fm=26&gp=0.jpg";
    private String url4 = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597727827948&di=63f6995f07f85682cfab93df826dce11&imgtype=0&src=http%3A%2F%2Fpic1.win4000.com%2Fpic%2F4%2F26%2F1179487672.jpg";

    private List<String> bannerInfos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_1, container, false);
        fvBanner = view.findViewById(R.id.fv_banner);
        bannerUrls = new ArrayList<>();
        Data1();
        initBanner(bannerInfos);

        return view;
    }

    private void initBanner(List<String> bannerInfos) {
        bannerUrls.clear();
        fvBanner.pause();
        for (int i = 0; i < bannerInfos.size(); i++) {
            bannerUrls.add(bannerInfos.get(i));
        }
//        for (BannerInfoBean bannerInfoBean : bannerInfos) {
//            bannerUrls.add(bannerInfoBean.getUrl());
//        }
//        fvBanner.setBannerPageClickListener(bannerPageClickListener);
        // 设置数据
        fvBanner.setPages(bannerUrls, new MZHolderCreator<BannerViewHolder>() {
            @Override
            public BannerViewHolder createViewHolder() {
                return new BannerViewHolder();
            }
        });
        fvBanner.setPageTransformer(new CustomTransformer());
        fvBanner.start();

    }

    private void Data1() {
        bannerInfos = new ArrayList<>();
        bannerInfos.add(url1);
        bannerInfos.add(url2);
        bannerInfos.add(url3);
        bannerInfos.add(url4);
    }

    private class BannerViewHolder implements MZViewHolder<String> {
        private RoundImageView mImageView;

        @Override
        public View createView(Context context) {
            // 返回页面布局
            mImageView = new RoundImageView(context);
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);// X和Y方向都填满
            mImageView.setType(RoundImageView.TYPE_ROUND);
            mImageView.setBorderRadius(8);
            mImageView.setImageResource(R.mipmap.place);
            return mImageView;
        }

        @Override
        public void onBind(Context context, int position, String data) {
            // 数据绑定
//            GlideImgManager.loadImage(context, data, R.mipmap.place, R.mipmap.place, mImageView);
            Glide.with(context)
                    .asBitmap()
                    .load(data)
                    .into(new ImageViewTarget<Bitmap>(mImageView) {
                        @Override
                        public void onLoadStarted(@Nullable Drawable placeholder) {
                            super.onLoadStarted(placeholder);

                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);

                        }

                        @Override
                        protected void setResource(@Nullable Bitmap resource) {
                            mImageView.setImageBitmap(resource);
                        }
                    });


//            Glide.with(getActivity()).load(data).asBitmap().into(new SimpleTarget<Bitmap>() {
//                @Override
//                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                    mImageView.setImageBitmap(resource);
//                }
//            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        fvBanner.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        fvBanner.pause();
    }


    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }
}
