package com.exam.student.applinks.webviewdemo;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.exam.student.applinks.R;
import com.just.agentweb.base.BaseAgentWebActivity;

import org.json.JSONObject;

/**
 * @author：luck
 * @data：2019/12/20 晚上 23:12
 * @描述: Demo
 */

public class JSDemoWebviewActivity extends BaseAgentWebActivity {
    private TextView mTitleTextView;
    private Handler mHandler;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_js);
        mTitleTextView = (TextView) this.findViewById(R.id.toolbar_title);
        mHandler = new Handler(Looper.getMainLooper());
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mAgentWeb.getJsAccessEntrace().quickCallJs("activitySetting", "0");
            }
        }, 200);

        findViewById(R.id.callJsNoParamsButton).setOnClickListener(mOnClickListener);
        findViewById(R.id.callJsOneParamsButton).setOnClickListener(mOnClickListener);
        findViewById(R.id.callJsMoreParamsButton).setOnClickListener(mOnClickListener);
        findViewById(R.id.jsJavaCommunicationButton).setOnClickListener(mOnClickListener);
    }

    @Nullable
    @Override
    public String getUrl() {
        return getIntent().getStringExtra(URL_KEY);
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.callJsNoParamsButton:
//                    mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroid");
                    mAgentWeb.getJsAccessEntrace().quickCallJs("activitySetting", "0");
                    break;
                case R.id.callJsOneParamsButton:
                    mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroidParam", "Hello ! Agentweb");
                    break;
                case R.id.callJsMoreParamsButton:
                    mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroidMoreParams", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            Log.i("Info", "value:" + value);
                        }
                    }, getJson());
                    break;
                case R.id.jsJavaCommunicationButton:
                    mAgentWeb.getJsAccessEntrace().quickCallJs("callByAndroidInteraction", "你好Js");
                    break;
            }
        }
    };


    private String getJson() {
        String result = "";
        try {
            JSONObject mJSONObject = new JSONObject();
            mJSONObject.put("id", 1);
            mJSONObject.put("name", "Agentweb");
            mJSONObject.put("age", 18);
            result = mJSONObject.toString();
        } catch (Exception e) {

        }
        return result;
    }

    @NonNull
    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.container);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb != null && mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected int getIndicatorColor() {
        return Color.parseColor("#ff0000");
    }

    @Override
    protected void setTitle(WebView view, String title) {
        super.setTitle(view, title);
        if (!TextUtils.isEmpty(title)) {
            if (title.length() > 20) {
                title = title.substring(0, 20).concat("...");
            }
        }
        mTitleTextView.setText(title);
    }

    @Override
    protected int getIndicatorHeight() {
        return 3;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //这个地方可以走广播
//        ToastUtils.showLong("当前界面关闭了");
    }
}
