package com.exam.student.applinks.basedemo.network;


import com.exam.student.applinks.basedemo.bean.VersionInfoBean;
import com.haier.cellarette.libretrofit.common.ResponseSlbBean1;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * 描述：
 * - 账户相关接口
 * 创建人：baoshengxiang
 * 创建时间：2017/7/13
 */
public interface AccountService {
    @FormUrlEncoded
    @POST("http://doc.znclass.com" + ApiInterface.QUERY_VERSION_INFO)
    Call<ResponseSlbBean1<VersionInfoBean>> queryVersion(@Field("programId") String programId, @Field("type") String type);
}
