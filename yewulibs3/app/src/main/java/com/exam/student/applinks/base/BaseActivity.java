package com.exam.student.applinks.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Image;
import android.media.ImageReader;
import android.media.SoundPool;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.exam.student.applinks.App;
import com.exam.student.applinks.R;
import com.exam.student.applinks.countdowndemo.pop.CountDownOverPop;
import com.exam.student.applinks.countdowndemo.pop.CountDownPop;
import com.exam.student.applinks.countdowndemo.server.TimerService;
import com.exam.student.applinks.easypermissionsdemo.PermissionActivity;
import com.exam.student.applinks.recyclerDemo.RecyclerActivity;
import com.exam.student.applinks.util.AndroidUtil;
import com.exam.student.applinks.util.FileUtil;
import com.exam.student.applinks.util.ServiceUtil;
import com.exam.student.applinks.util.UiUtils;
import com.exam.student.applinks.zxingdemo.SaomaActDemo;
import com.example.baselibrary.base.BaseAppManager;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;

import cn.hzw.doodle.DoodleActivity;
import cn.hzw.doodle.DoodleParams;
import cn.hzw.doodle.DoodleView;
import me.jessyan.autosize.AutoSizeCompat;

public abstract class BaseActivity extends AppCompatActivity {

    protected Application appContext;
    protected BaseActivity activity;
    protected float density;
    protected boolean hasOnCreate = false;
    protected CountDownPop countDownPop;

    private TimerService timerService;
    private ServiceConnection timerConn;

    public static final int REQ_CODE_DOODLE = 101;
    private MyHandler myHandler;

    private long mCurrentMs = System.currentTimeMillis();

    @Override
    public Resources getResources() {
        //需要升级到 v1.1.2 及以上版本才能使用 AutoSizeCompat
        AutoSizeCompat.autoConvertDensityOfGlobal((super.getResources()));//如果没有自定义需求用这个方法
        AutoSizeCompat.autoConvertDensity((super.getResources()), 667, false);//如果有自定义需求就用这个方法
        return super.getResources();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            //透明状态栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }

        BaseAppManager.getInstance().add(this);
        appContext = getApplication();
        activity = this;
        density = getResources().getDisplayMetrics().density;
        myHandler = new MyHandler();
        mMediaProjectionManager = (MediaProjectionManager)
                activity.getSystemService(Context.MEDIA_PROJECTION_SERVICE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ActivityManager.addActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ActivityManager.removeActivity(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onResume() {
        super.onResume();
        ActivityManager.addActivity(this);
        MobclickAgent.onResume(this);
        View contentView = getContentView(activity);
//        if (hasOnCreate) {
//            initPop(contentView);
//        } else {
//            addTools(contentView);
//        }
        if (!App.showCountDown) {
            dismissCountDownPop();
        }
        hasOnCreate = true;
    }


    /*------------------涂鸦------------------*/
    private ImageReader imageReader;
    public MediaProjectionManager mMediaProjectionManager;
    public static final int REQUEST_MEDIA_PROJECTION_CAPTURE = 1001;

    /**
     * 截屏
     */
    @SuppressLint("WrongConstant")
    public void getScreen() {
        if (App.mMediaProjection == null) {
            return;
        }
        imageReader = ImageReader.newInstance(UiUtils.getScreenW(), UiUtils.getScreenH() + AndroidUtil.getDaoHangHeight(activity), 0x1, 1);
        final VirtualDisplay mVirtualDisplay = App.mMediaProjection.createVirtualDisplay("ScreenCapture",
                UiUtils.getScreenW(), UiUtils.getScreenH() + AndroidUtil.getDaoHangHeight(activity), 1,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY,
                imageReader.getSurface(), null, null);
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startCapture();
              /*          mVirtualDisplay.release();
                        Intent intent = new Intent(activity, PaletteActivity.class);
                        intent.putExtra("type", "tuya");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);*/

                        // 涂鸦参数
                        DoodleParams params = new DoodleParams();
                        params.mIsFullScreen = true;
                        // 图片路径
                        try {
                            params.mImagePath = FileUtil.getResPath();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // 初始画笔大小
                        params.mPaintUnitSize = DoodleView.DEFAULT_SIZE;
                        // 画笔颜色
                        params.mPaintColor = Color.RED;
                        // 是否支持缩放item
                        params.mSupportScaleItem = true;
                        // 启动涂鸦页面
                        DoodleActivity.startActivityForResult(activity, params, REQ_CODE_DOODLE);

                    }
                });
            }
        }, 1000);
    }

    /**
     * 获取截屏图片
     */
    private void startCapture() {
        Image image = null;
        while (image == null) {
            image = imageReader.acquireLatestImage();
        }
        int width = image.getWidth();
        int height = image.getHeight();
        final Image.Plane[] planes = image.getPlanes();
        final ByteBuffer buffer = planes[0].getBuffer();
        int pixelStride = planes[0].getPixelStride();
        int rowStride = planes[0].getRowStride();
        int rowPadding = rowStride - pixelStride * width;
        Bitmap bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height - AndroidUtil.getDaoHangHeight(activity));
        image.close();
        if (bitmap != null) {
            try {
                File file = new File(FileUtil.getResPath(), "capture.png");
                if (file.exists()) {
                    FileUtil.delete(file);
                }
                file.createNewFile();
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MEDIA_PROJECTION_CAPTURE) {
            App.mMediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
            getScreen();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    /*------------------涂鸦结束------------------*/

    /*------------------倒计时------------------*/

    /**
     * 绑定倒计时服务
     */
    private void bindTimerService(final int time) {
        if (timerConn != null && timerService != null) {
            timerService.setTime(time);
            startTimer();
            return;
        }
        Intent service = new Intent(activity, TimerService.class);
        timerConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerService.TimerBinder timerBinder = (TimerService.TimerBinder) service;
                timerService = timerBinder.getService();
                timerService.setTime(time);
                startTimer();
            }
        };
        if (!ServiceUtil.isServiceRunning(appContext, TimerService.class.getName())) {
            startService(service);
        }
        bindService(service, timerConn, Context.BIND_AUTO_CREATE);
    }

    /**
     * 绑定倒计时服务
     */
    private void bindTimerService() {
        if (timerConn != null) {
            return;
        }
        Intent service = new Intent(activity, TimerService.class);
        timerConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                TimerService.TimerBinder timerBinder = (TimerService.TimerBinder) service;
                timerService = timerBinder.getService();
                startTimer();
            }
        };
        if (!ServiceUtil.isServiceRunning(appContext, TimerService.class.getName())) {
            startService(service);
        }
        bindService(service, timerConn, Context.BIND_AUTO_CREATE);
    }

    /**
     * 开始计时
     */
    private void startTimer() {
        if (timerService != null) {
            timerService.setHandler(myHandler);
            if (timerService != null) {
                timerService.startTimer();
            }
        }
    }

    private class MyHandler extends Handler {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TimerService.MSG_TIMER_RUNNING:
                    int recLen = (int) msg.obj;
                    if (countDownPop != null) {
                        countDownPop.setTime(recLen);
                    }
                    break;
                case TimerService.MSG_TIMER_FINISHED:
                    dismissCountDownPop();
                    // 显示时间到
                    CountDownOverPop countDownOverPop = new CountDownOverPop(activity);
                    countDownOverPop.showPopupWindow(getContentView(activity));
                    // 播放声音
//                    SoundPool sp=new SoundPool(10, AudioManager.STREAM_MUSIC,0);
                    SoundPool.Builder sb = new SoundPool.Builder();
                    sb.setMaxStreams(10);
                    AudioAttributes.Builder audioAttributesBuilder = new AudioAttributes.Builder();
                    audioAttributesBuilder.setLegacyStreamType(AudioManager.STREAM_MUSIC);
                    AudioAttributes aa = audioAttributesBuilder.build();
                    sb.setAudioAttributes(aa);//默认是media类型，其他的可以看看源码。都有
                    final SoundPool sp = sb.build();
                    final int count_over = sp.load(activity, R.raw.count_over, 1);
                    sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                        @Override
                        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
//                            soundPool.play(count_over, 0.6f, 0.6f, 1, 0, 1);
                            sp.play(count_over, 1, 1, 1, 0, 1);
                        }
                    });
                    break;
                default:
                    break;
            }
            super.

                    handleMessage(msg);
        }
    }


    private void initPop(View contentView) {
        if (activity instanceof SaomaActDemo
                || activity instanceof PermissionActivity
                || activity instanceof RecyclerActivity) {
            return;
        }
        if (App.showCountDown) {
            showCountDownPop();
        }
    }

    public void showCountDownPop(int time) {
        initCountDownPop();
        bindTimerService(time);
    }

    private void initCountDownPop() {
        if (countDownPop == null) {
            countDownPop = new CountDownPop(activity);
        }
        countDownPop.showPopupWindow(getContentView(activity));
    }


    /**
     * 显示倒计时窗口(记时弹出框)
     */
    private void showCountDownPop() {
        initCountDownPop();
        bindTimerService();
    }

    /**
     * 移除倒计时窗口
     */
    private void dismissCountDownPop() {
        if (countDownPop != null) {
            countDownPop.dismissPop();
            countDownPop = null;
        }
        if (timerConn != null) {
            unbindService(timerConn);
            timerConn = null;
        }
    }

    /*------------------倒计时------------------*/
    @Override
    protected void onPause() {
        super.onPause();
        dismissCountDownPop();
        ActivityManager.removeActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        removeTools();
        MobclickAgent.onPause(this);
        ActivityManager.removeActivity(this);
        if (myHandler != null) {
            myHandler.removeCallbacksAndMessages(null);
        }

    }

    /**
     * 获取activity的ContentView
     *
     * @param context
     * @return
     */
    public View getContentView(Activity context) {
        return ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
    }

    public String getIdentifier() {
        return getClass().getName() + mCurrentMs;
    }

}