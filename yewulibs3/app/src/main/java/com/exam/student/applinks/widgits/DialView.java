package com.exam.student.applinks.widgits;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;

import com.exam.student.applinks.R;

import java.util.List;
import java.util.regex.Pattern;

public class DialView extends RelativeLayout {
    LinearLayout dialpad_0, dialpad_1, dialpad_2, dialpad_3, dialpad_4, dialpad_5, dialpad_6, dialpad_7, dialpad_8, dialpad_9;
    TextView dialpad_star, dialpad_pound;
    ImageView dialpad_delete;//删除
    List<View> mDialpadViews;
    AppCompatEditText mDialpadInput;
    ImageView fab_call, fab_set;


    public interface DialViewListener {

        /**
         * 输入内容变化
         */
        void inputChange();

        /**
         * 点击拨号按键
         */
        void onCall();

        /**
         * 点击设置按键
         */
        void onSetting();

//        /**
//         * 长按事件
//         */
//        void onLongEvent(String number);

    }

    private DialViewListener mDialViewListener;

    public void setDialViewListener(DialViewListener DialViewListener) {
        this.mDialViewListener = DialViewListener;
    }

    public DialView(Context context) {
        this(context, null);
    }

    public DialView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DialView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View.inflate(context, R.layout.dial_view, this);
        dialpad_0 = findViewById(R.id.dialpad_0);
        dialpad_1 = findViewById(R.id.dialpad_1);
        dialpad_2 = findViewById(R.id.dialpad_2);
        dialpad_3 = findViewById(R.id.dialpad_3);
        dialpad_4 = findViewById(R.id.dialpad_4);
        dialpad_5 = findViewById(R.id.dialpad_5);
        dialpad_6 = findViewById(R.id.dialpad_6);
        dialpad_7 = findViewById(R.id.dialpad_7);
        dialpad_8 = findViewById(R.id.dialpad_8);
        dialpad_9 = findViewById(R.id.dialpad_9);
        dialpad_star = findViewById(R.id.dialpad_star);
        dialpad_pound = findViewById(R.id.dialpad_pound);
        dialpad_delete = findViewById(R.id.dialpad_delete);
        fab_set = findViewById(R.id.fab_set);
        mDialpadInput = findViewById(R.id.dialpad_txt);
        fab_call = findViewById(R.id.fab_call);
        mDialpadInput.setInputType(InputType.TYPE_NULL);
        dialpad_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_0.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_1.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_2.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_3.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_4.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_5.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_6.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_7.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_8.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
        dialpad_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) dialpad_9.getChildAt(0);
                mDialpadInput.append(textView.getText());
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });

        fab_call.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialViewListener != null) {
                    mDialViewListener.onCall();
                }
            }
        });
        fab_set.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialViewListener != null) {
                    mDialViewListener.onSetting();
                }
            }
        });
        dialpad_delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = mDialpadInput.getText().toString();
                if (!TextUtils.isEmpty(content)) {
                    mDialpadInput.setText(content.substring(0, content.length() - 1));
                }
                if (mDialViewListener != null) {
                    mDialViewListener.inputChange();
                }
            }
        });
    }


    /**
     * 获取输入框内容
     *
     * @return
     */
    public String getNumber() {
        return mDialpadInput.getText().toString();
    }

    /**
     * 清空输入框
     */
    public void cleanDialViewInput() {
        mDialpadInput.setText("");
    }

    /**
     * 设置输入框内容
     *
     * @param input
     */
    public void setDialViewInput(String input) {
        Pattern pattern = Pattern.compile("[a-zA-z0-9]{1,15}");
        if (pattern.matcher(input).matches()) {
            mDialpadInput.setText(input);
        } else {
            return;
        }
    }
//    @OnLongClick({R.id.dialpad_0, R.id.dialpad_1, R.id.dialpad_2, R.id.dialpad_3, R.id.dialpad_4
//            , R.id.dialpad_5, R.id.dialpad_6, R.id.dialpad_7, R.id.dialpad_8, R.id.dialpad_9})
//    boolean onLongDialpad(ViewGroup viewGroup) {
//        TextView textView = (TextView) viewGroup.getChildAt(0);
//        if (mDialViewListener != null) {
//            mDialViewListener.onLongEvent(textView.getText().toString());
//        }
//        return true;
//    }
}
