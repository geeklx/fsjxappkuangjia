package com.exam.student.applinks.androidtreeview.holder;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.exam.student.applinks.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by Bogdan Melnychuk on 2/13/15.
 */
public class ContentHolder extends TreeNode.BaseNodeViewHolder<IconTreeItem> {
    TextView tvicon;
    TextView tvValue;
    LinearLayout llBg;

    public ContentHolder(Context context) {
        super(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View createNodeView(TreeNode node, IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_tree_content, null, false);
        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);

        tvValue = (TextView) view.findViewById(R.id.node_value);
        tvValue.setText(value.text);

        tvicon = (TextView) view.findViewById(R.id.tvicon);
        tvicon.setText(value.icon);//默认 1

        if (node.isLeaf()) {
            tvicon.setVisibility(View.INVISIBLE);
        }

//        int treeviewPadding = ResourcesUtil.getDimensionPixelSize(R.dimen.treeview_padding);


        int treeviewPadding = 25;
        int treeviewleft;
        treeviewleft = node.getLevel() * treeviewPadding;
//        view.setPadding(treeviewPadding, 0, 0, 0);

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) llBg.getLayoutParams();
        llBg.setPadding(treeviewleft, 0, 0, 0);
        lp.weight = 400 - treeviewleft;

        view.setLayoutParams(lp);

//        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) llBg.getLayoutParams();
//        lp.width = 300+treeviewleft;
//        view.setLayoutParams(lp);
//        //view.setX(x);
//        view.requestLayout();

//        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tvValue.getLayoutParams();
//        Log.e("eeeetreeviewleft---", treeviewleft+"");
//        lp.weight=400-(treeviewleft);
////        lp.leftMargin=
//        tvValue.setLayoutParams(lp);


        return view;
    }

    public TextView setSelected() {
        tvValue.setTextColor(Color.WHITE);
        tvValue.setBackgroundColor(Color.RED);
        llBg.setBackgroundColor(Color.RED);
        return tvValue;
    }

    public TextView setUnselected() {
        tvValue.setTextColor(Color.BLACK);
        tvValue.setBackgroundColor(Color.TRANSPARENT);
        llBg.setBackgroundColor(Color.TRANSPARENT);
        return tvValue;
    }


    @Override
    public void toggle(boolean active) {
        tvicon.setText(active ? "2" : "1");
    }

}
