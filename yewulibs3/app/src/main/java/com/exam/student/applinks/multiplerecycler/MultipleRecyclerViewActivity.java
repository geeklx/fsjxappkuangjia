package com.exam.student.applinks.multiplerecycler;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MultipleRecyclerViewActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private TextView mTvCount;
    private List<String> datas;
    private List<String> selectDatas = new ArrayList<>();
    private MultiAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_recycler_view);
        initData();
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mTvCount = (TextView) findViewById(R.id.tv_count);

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setFocusable(false);

        mAdapter = new MultiAdapter(datas);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!mAdapter.isSelected.get(position)) {
                    mAdapter.isSelected.put(position, true); // 修改map的值保存状态
                    mAdapter.notifyItemChanged(position);
                    selectDatas.add(datas.get(position));

                } else {
                    mAdapter.isSelected.put(position, false); // 修改map的值保存状态
                    mAdapter.notifyItemChanged(position);
                    selectDatas.remove(datas.get(position));
                }

                mTvCount.setText("已选中" + selectDatas.size() + "项");
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });
    }

    /**
     * 全选
     *
     * @param view
     */
    public void all(View view) {
        selectDatas.clear();
        for (int i = 0; i < datas.size(); i++) {
            mAdapter.isSelected.put(i, true);
            selectDatas.add(datas.get(i));
        }
        mAdapter.notifyDataSetChanged();

        mTvCount.setText("已选中" + selectDatas.size() + "项");
    }

    /**
     * 反选
     *
     * @param view
     */
    public void inverse(View view) {
        for (int i = 0; i < datas.size(); i++) {
            if (mAdapter.isSelected.get(i)) {
                mAdapter.isSelected.put(i, false);
                selectDatas.remove(datas.get(i));
            } else {
                mAdapter.isSelected.put(i, true);
                selectDatas.add(datas.get(i));
            }
        }
        mAdapter.notifyDataSetChanged();
        mTvCount.setText("已选中" + selectDatas.size() + "项");

    }

    /**
     * 取消已选
     *
     * @param view
     */
    public void cancel(View view) {
        for (int i = 0; i < datas.size(); i++) {
            if (mAdapter.isSelected.get(i)) {
                mAdapter.isSelected.put(i, false);
                selectDatas.remove(datas.get(i));
            }
        }
        mAdapter.notifyDataSetChanged();
        mTvCount.setText("已选中" + selectDatas.size() + "项");
    }

    private String json;
    List<TextList> listmutiple;
    TextList textList;

    /**
     * 选中拿到的数据
     *
     * @param view
     */
    public void takevalue(View view) {
        listmutiple = new ArrayList<>();
        for (int i = 0; i < datas.size(); i++) {
            if (mAdapter.isSelected.get(i)) {
                textList = new TextList();
                mAdapter.isSelected.put(i, true);
                textList.setName(datas.get(i));
                listmutiple.add(textList);
            }
        }
        for (int i = 0; i < listmutiple.size(); i++) {
            json = new Gson().toJson(listmutiple);
        }
        ToastUtils.showLong(json);
    }


    private void initData() {
        datas = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            datas.add("测试" + i);
        }
    }
}
