package com.exam.student.applinks.waterfallflowrecycler;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.exam.student.applinks.R;
import com.exam.student.applinks.waterfallflowrecycler.adapter.Shoppingadapter;
import com.exam.student.applinks.waterfallflowrecycler.bean.OneflowBean;

import java.util.ArrayList;
import java.util.List;

public class WaterfallFlowActivity extends AppCompatActivity {
    private RecyclerView recyclerViewAiznxuexi;//底部显示

    private Shoppingadapter shoppingadapter;
    private List<OneflowBean> mDataTablayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waterfall_flow);
        recyclerViewAiznxuexi = findViewById(R.id.rv_shopping);
        onclick();
        donetworkketangxuexi();
    }

    private void donetworkketangxuexi() {
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            mDataTablayout.add(new OneflowBean(String.valueOf(i), String.valueOf(i)));
        }
        shoppingadapter.setNewData(mDataTablayout);
    }

    private void onclick() {
        /*课堂学习*/
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.invalidateSpanAssignments();
        recyclerViewAiznxuexi.setLayoutManager(staggeredGridLayoutManager);
        shoppingadapter = new Shoppingadapter();
        recyclerViewAiznxuexi.setAdapter(shoppingadapter);
        recyclerViewAiznxuexi.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            }

            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                int itemViewId = view.getId();
                switch (itemViewId) {
                    case R.id.ll_new_comment_product:
                        ToastUtils.showLong("点击了" + position);
                        break;
                }
            }
        });

    }
}