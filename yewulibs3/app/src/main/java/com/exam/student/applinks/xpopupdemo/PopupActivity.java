package com.exam.student.applinks.xpopupdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.CorrectVo;
import com.exam.student.applinks.R;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.interfaces.OnSelectListener;

import java.util.ArrayList;
import java.util.List;

public class PopupActivity extends AppCompatActivity {
    CustomDrawerPopupView customDrawerPopupView;
    private int checkedPos = 1;//默认选中

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);
        customDrawerPopupView = new CustomDrawerPopupView(this);
        /*弹出activity*/
        findViewById(R.id.rl_dialog_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppUtils.getAppPackageName() + ".hs.act.slbapp.AdCommImgActivity");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id1", "你想要的东西");
                startActivity(intent);
            }
        });
        /*侧滑*/
        findViewById(R.id.iv3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .popupPosition(PopupPosition.Right)//右边
//                        .hasStatusBarShadow(true) //启用状态栏阴影
                        .asCustom(customDrawerPopupView)
                        .show();

            }
        });


        /*中间点击     asAttachList1 传 checkedPos  有默认选中*/
        findViewById(R.id.iv2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .hasShadowBg(false)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .offsetY(20)
                        .offsetX(-120)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                        .atView(v)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                        .asAttachList1(new String[]{"班级", "班级2", "班级3", "班级4"},
                                new int[]{},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        checkedPos = position;
                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                    }
                                }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item, checkedPos)
                        .show();
            }
        });

        /*右侧点击      asAttachList无默认选中   */
        findViewById(R.id.iv1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new XPopup.Builder(PopupActivity.this)
                        .hasShadowBg(false)
                        .isDestroyOnDismiss(true) //对于只使用一次的弹窗，推荐设置这个
                        .offsetY(20)
                        .offsetX(40)
//                        .popupPosition(PopupPosition.Top) //手动指定弹窗的位置
                        .atView(v)  // 依附于所点击的View，内部会自动判断在上方或者下方显示
                        .asAttachList(new String[]{"移除班级", "解除绑定"},
                                new int[]{},
                                new OnSelectListener() {
                                    @Override
                                    public void onSelect(int position, String text) {
                                        ToastUtils.showShort("click " + text + "\n pos" + position);
                                    }
                                }, R.layout.popup_adapter_rv, R.layout.popup_adapter_item)
                        .show();
            }


        });
    }

    /**
     * 集合转 String[]数组
     */
    private String[] getStringData(List<String> list) {
        String[] strArray = list.toArray(new String[list.size()]);
        return strArray;
    }

    private String[] getStringBean(CorrectVo correctVo) {
        List<String> list = new ArrayList<>();
        for (CorrectVo.DataBean.CorrectListBean correctListBean : correctVo.getData().getCorrectList()) {
            list.add(String.valueOf(correctListBean.getRate()));
        }

        String[] strArray = list.toArray(new String[list.size()]);
        return strArray;
    }


}
