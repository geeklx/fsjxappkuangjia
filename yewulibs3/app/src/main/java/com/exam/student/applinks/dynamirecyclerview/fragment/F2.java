package com.exam.student.applinks.dynamirecyclerview.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.exam.student.applinks.R;
import com.exam.student.applinks.base.BaseFragment;
import com.exam.student.applinks.base.FragmentHelper;
import com.exam.student.applinks.dynamirecyclerview.adapter.OrderFragmentPagerAdapter;
import com.exam.student.applinks.dynamirecyclerview.adapter.Tablayoutdapter;
import com.exam.student.applinks.newwork.Presenter.HCategoryPresenter;
import com.exam.student.applinks.newwork.bean.HCategoryBean;
import com.exam.student.applinks.newwork.bean.HCategoryBean1;
import com.exam.student.applinks.newwork.bean.HLunbotuBean1;
import com.exam.student.applinks.newwork.bean.OneBean1;
import com.exam.student.applinks.newwork.view.HCategoryView;
import com.exam.student.applinks.widgits.XRecyclerView;
import com.exam.student.applinks.widgits.tablayout.ViewPagerSlide;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


public class F2 extends BaseFragment implements HCategoryView {
    private ImageView ivBack1Order;
    private TextView tvCenterContent;
    private TextView tvRight;
    private ImageView shareIv;
    private LinearLayout llRefresh1Order;
    private XRecyclerView recyclerView1Order11;
    private ViewPagerSlide viewpagerMy1Order;


    private String current_id;
    private String ids;
    private List<HLunbotuBean1> mListbanner1;
    private HCategoryPresenter presenter1;
    protected boolean enscrolly;
    private Tablayoutdapter mAdapter11;
    private List<OneBean1> mDataTablayout;
    private OrderFragmentPagerAdapter orderFragmentPagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_2, container, false);
        ivBack1Order = (ImageView) view.findViewById(R.id.iv_back1_order);
        tvCenterContent = (TextView) view.findViewById(R.id.tv_center_content);
        tvRight = (TextView) view.findViewById(R.id.tv_right);
        shareIv = (ImageView) view.findViewById(R.id.shareIv);
        llRefresh1Order = (LinearLayout) view.findViewById(R.id.ll_refresh1_order);
        recyclerView1Order11 = (XRecyclerView) view.findViewById(R.id.recycler_view1_order11);
        viewpagerMy1Order = (ViewPagerSlide) view.findViewById(R.id.viewpager_my1_orderf2);
        initviw();
        return view;
    }

    private void initviw() {
        ivBack1Order.setVisibility(View.GONE);
        tvCenterContent.setText("我的课程");
        onclick();
        //
        mListbanner1 = new ArrayList<>();
        presenter1 = new HCategoryPresenter();
        presenter1.onCreate(this);
        donetwork();
    }

    private void donetwork() {
        //
        HCategoryBean hCategoryBean = new HCategoryBean();
        List<HCategoryBean1> mDataTablayout1 = new ArrayList<>();
        mDataTablayout1.add(new HCategoryBean1("1", "课程任务1"));
        mDataTablayout1.add(new HCategoryBean1("2", "课程任务2"));
        mDataTablayout1.add(new HCategoryBean1("3", "课程任务3"));
        hCategoryBean.setList(mDataTablayout1);
        //
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        Log.e("---geekyun----", current_id);
    }


    private void onclick() {
        //
        recyclerView1Order11.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
//        JackSnapHelper mLinearSnapHelper = new JackSnapHelper(JackSnapHelper.TYPE_SNAP_START);
//        mLinearSnapHelper.attachToRecyclerView(recyclerView1Order11);
        mDataTablayout = new ArrayList<>();
        mAdapter11 = new Tablayoutdapter();
        recyclerView1Order11.setAdapter(mAdapter11);
        mAdapter11.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                OneBean1 bean1 = (OneBean1) adapter.getData().get(position);
//                set_footer_change(bean1);
                //
                current_id = bean1.getTab_id();
                // 这段代码如果单独用tablayout的时候 就需要 因为tablayout会自动执行
//                if (!once) {
//                    once = true;
//                    return;
//                }
                if (null == current_id) {
                    return;
                }
                if (bean1.isEnable()) {
                    // 不切换当前的item点击 刷新当前页面
                    return;
                }
                Log.e("---geekyun----", current_id);
                Log.e("---geekyun-position---", position + "");
                viewpagerMy1Order.setCurrentItem(position, true);
            }
        });
    }

    private void init_viewp(List<OneBean1> mlist) {
        //
        if (mlist == null || mlist.size() == 0) {
            return;
        }
        List<Fragment> mFragmentList = new ArrayList<>();
        for (int i = 0; i < mlist.size(); i++) {
            Bundle bundle = new Bundle();
            bundle.putString("id", mlist.get(i).getTab_id());
            if (i == 0) {
                F2NewItem1 fragment1 = FragmentHelper.newFragment(F2NewItem1.class, bundle);
                mFragmentList.add(fragment1);
            } else if (i == 2) {
                F2NewItem3 fragment1 = FragmentHelper.newFragment(F2NewItem3.class, bundle);
                mFragmentList.add(fragment1);
            } else {
                F2NewItem2 fragment1 = FragmentHelper.newFragment(F2NewItem2.class, bundle);
                mFragmentList.add(fragment1);
            }
        }
        orderFragmentPagerAdapter = new OrderFragmentPagerAdapter(getActivity().getSupportFragmentManager(), getActivity(), mFragmentList);
        viewpagerMy1Order.setAdapter(orderFragmentPagerAdapter);
        viewpagerMy1Order.setOffscreenPageLimit(4);
        viewpagerMy1Order.setScroll(true);
        viewpagerMy1Order.setCurrentItem(0, false);
        viewpagerMy1Order.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                curPosition = position;
                OneBean1 bean1 = mAdapter11.getData().get(position);
                set_footer_change(bean1);
            }
        });
    }

    /**
     * 业务逻辑部分
     */

    private void initList() {
        for (int i = 0; i < mAdapter11.getData().size(); i++) {
            OneBean1 item = mAdapter11.getData().get(i);
            if (item.isEnable()) {
                item.setEnable(false);
            }
        }
    }


    private void set_footer_change(OneBean1 model) {
        if (model.isEnable()) {
            // 不切换当前的item点击 刷新当前页面
            return;
        } else {
            // 切换到另一个item
            //设置为选中
            initList();
            model.setEnable(true);
            mAdapter11.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        presenter1.onDestory();
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * 底部点击bufen
     *
     * @param cateId
     * @param isrefresh
     */
    public void getCate(String cateId, boolean isrefresh) {
        if (!isrefresh) {
            // 从缓存中拿出头像bufen
            return;
        }
    }

    /**
     * 当切换底部的时候通知每个fragment切换的id是哪个bufen
     *
     * @param cateId
     */
    public void give_id(String cateId) {
//        Toasty.normal(getActivity(), "cateId=" + cateId + "下拉刷新啦").show();
    }

    @Override
    public void OnCategorySuccess(HCategoryBean hCategoryBean) {
        mDataTablayout = new ArrayList<>();
        for (int i = 0; i < hCategoryBean.getList().size(); i++) {
            if (i == 0) {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), true));
            } else {
                mDataTablayout.add(new OneBean1(hCategoryBean.getList().get(i).getCode(), hCategoryBean.getList().get(i).getName(), false));
            }
        }
        current_id = mDataTablayout.get(0).getTab_id();
        mAdapter11.setNewData(mDataTablayout);
        init_viewp(mDataTablayout);
        Log.e("---geekyun----", current_id);
    }

    @Override
    public void OnCategoryNodata(String bean) {

    }

    @Override
    public void OnCategoryFail(String msg) {

    }
}
