package com.exam.student.applinks.dynamirecyclerview.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.basedemo.BaseActFragment;


public class F2NewItem2 extends BaseActFragment {

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_newitem2;
    }


    @Override
    protected void setup(View rootView, @Nullable Bundle savedInstanceState) {
        super.setup(rootView, savedInstanceState);
    }

    protected ViewGroup getAgentWebParent() {
        LogUtils.e("BaseActFragment------getAgentWebParent");
        return (ViewGroup) getActivity().findViewById(R.id.ll_base_container2);
    }

    @Nullable
    protected String getUrl() {
        Bundle bundle = this.getArguments();
        String target = bundle.getString("url_key");
        LogUtils.e("targetaaaaaaa=" + target);
        if (TextUtils.isEmpty(target)) {
            target = "http://www.jd.com/";
        }
        return target;
    }

    @Override
    public void call(Object value) {
        ids = (String) value;
        ToastUtils.showLong(ids);
        //
//        set_re_data();
    }
}
