package com.exam.student.applinks.basedemo;

import android.os.Bundle;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.R;
import com.exam.student.applinks.basedemo.bean.VersionInfoBean;
import com.exam.student.applinks.basedemo.presenter.CheckverionPresenter;
import com.exam.student.applinks.basedemo.view.CheckverionView;
import com.exam.student.applinks.webviewdemo.AndroidInterface;

public class BaseActDemoYewu extends BaseActDemo implements BaseOnClickListener, CheckverionView {
    CheckverionPresenter checkverionPresenter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_baseact_demo;
    }

    @Override
    protected void setup(@Nullable Bundle savedInstanceState) {
        super.setup(savedInstanceState);
        checkverionPresenter = new CheckverionPresenter();
        checkverionPresenter.onCreate(this);
        checkverionPresenter.checkVerion("3", "0");
        if (mAgentWeb != null) {
            //注入对象
            mAgentWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface(mAgentWeb, this));
            String target = getIntent().getStringExtra(URL_KEY);
            loadWebSite(target); // 刷新
        }
    }

    /* 重载业务部分*/
    @Override
    protected void donetwork() {
        super.donetwork();
        TitleShowHideState(5);
        setBaseOnClickListener(this);
    }

    @Override
    protected ViewGroup getAgentWebParent() {
        return (ViewGroup) this.findViewById(R.id.ll_base_container);
    }

    //个人中心
    @Override
    public void Titlegrzx() {
        ToastUtils.showLong("点击了个人中心");
    }

    @Override
    public void Titleshijian() {
        showCalendarDialog();
        BaseActDemo.setOnDisplayRefreshListener(new refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });
    }

    /*展开时间*/
    @Override
    public void Titlezankaishijian() {
        showCalendarDialog();
        BaseActDemo.setOnDisplayRefreshListener(new refreshOnDisplayListener() {
            @Override
            public void returnRefresh(String startTime, String endTime) {
                ToastUtils.showLong("开始时间-" + startTime + "结束时间-" + endTime);
                AgentwebRefresh("http://www.jd.com/");
            }
        });
    }

    //搜索
    @Override
    public void Titlesousuo() {
        ToastUtils.showLong("点击了搜索带刷新效果");
        AgentwebRefresh("http://www.jd.com/");
    }

    @Override
    public void Titletijiao() {
        ToastUtils.showLong("点击了提交");
    }

    @Override
    public void TitleDropdown() {
        ToastUtils.showLong("点击了下拉列表");
    }

    @Override
    public void OnUpdateVersionSuccess(VersionInfoBean versionInfoBean) {
        AgentwebRefresh("https://www.baidu.com/");
    }

    @Override
    public void OnUpdateVersionNodata(String bean) {
        ToastUtils.showLong("OnUpdateVersionNodata" + bean);
    }

    @Override
    public void OnUpdateVersionFail(String msg) {
        ToastUtils.showLong("OnUpdateVersionFail" + msg);
    }
}
