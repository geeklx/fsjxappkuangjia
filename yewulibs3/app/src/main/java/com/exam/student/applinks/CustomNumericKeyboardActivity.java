package com.exam.student.applinks;

import android.os.Bundle;
import com.blankj.utilcode.util.ToastUtils;
import com.exam.student.applinks.base.BaseActivity;
import com.exam.student.applinks.widgits.DialView;

public class CustomNumericKeyboardActivity extends BaseActivity {
    private DialView mdialView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard);
        mdialView = findViewById(R.id.dialView);
        mdialView.setDialViewListener(new DialView.DialViewListener() {
            @Override
            public void inputChange() {//input值
            }

            @Override
            public void onCall() {
                ToastUtils.showShort(mdialView.getNumber());
            }

            @Override
            public void onSetting() {
                ToastUtils.showShort("onSetting");
            }
        });
    }
}
