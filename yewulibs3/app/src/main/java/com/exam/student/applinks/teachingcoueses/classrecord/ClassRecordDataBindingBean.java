package com.exam.student.applinks.teachingcoueses.classrecord;

import java.io.Serializable;

public class ClassRecordDataBindingBean implements Serializable {
    private String name;//课程名称
    private String className;//上课班级
    private String createTime;//上课时间

    public ClassRecordDataBindingBean(String name, String className, String createTime) {
        this.name = name;
        this.className = className;
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


}
