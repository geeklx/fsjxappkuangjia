package com.lxj.xpopup.impl;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.easyadapter.EasyAdapter;
import com.lxj.easyadapter.MultiItemTypeAdapter;
import com.lxj.easyadapter.ViewHolder;
import com.lxj.xpopup.R;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.AttachPopupView;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.lxj.xpopup.widget.CheckView;
import com.lxj.xpopup.widget.VerticalRecyclerView;

import java.util.Arrays;

/**
 * Description: Attach类型的列表弹窗
 * Create by dance, at 2018/12/12
 */
public class AttachListPopupView1 extends AttachPopupView {
    RecyclerView recyclerView;
    protected int bindLayoutId;
    protected int bindItemLayoutId;

    /**
     *
     * @param context
     * @param bindLayoutId layoutId 要求layoutId中必须有一个id为recyclerView的RecyclerView
     * @param bindItemLayoutId itemLayoutId 条目的布局id，要求布局中必须有id为iv_image的ImageView，和id为tv_text的TextView
     */
    public AttachListPopupView1(@NonNull Context context, int bindLayoutId, int bindItemLayoutId) {
        super(context);
        this.bindLayoutId = bindLayoutId;
        this.bindItemLayoutId = bindItemLayoutId;
        addInnerContent();
    }

    @Override
    protected int getImplLayoutId() {
        return bindLayoutId == 0 ? R.layout._xpopup_attach_impl_list : bindLayoutId;
    }

    @Override
    protected void initPopupContent() {
        super.initPopupContent();
        recyclerView = findViewById(R.id.recyclerView);
        if(recyclerView instanceof VerticalRecyclerView){
            ((VerticalRecyclerView)recyclerView).setupDivider(popupInfo.isDarkTheme);
        }else {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        final EasyAdapter<String> adapter = new EasyAdapter<String>(Arrays.asList(data), bindItemLayoutId == 0 ? R.layout._xpopup_adapter_text : bindItemLayoutId) {
            @Override
            protected void bind(@NonNull ViewHolder holder, @NonNull String s, int position) {
                holder.setText(R.id.tv_text, s);
                if (iconIds != null && iconIds.length > position) {
                    holder.getView(R.id.iv_image).setVisibility(VISIBLE);
                    holder.getView(R.id.iv_image).setBackgroundResource(iconIds[position]);
                } else {
                    holder.getView(R.id.iv_image).setVisibility(GONE);
                }
                View check = holder.getView(R.id.check_view);
                if (check!=null) check.setVisibility(GONE);

                if (checkedPosition != -1) {
                    if (holder.getView(R.id.ll_bg) != null) {
                        if (position==checkedPosition){
                            holder.getView(R.id.ll_bg).setBackgroundColor(Color.parseColor("#FEF1EB"));
                            ((TextView)holder.getView(R.id.tv_text)).setTextColor(Color.parseColor("#FA541C"));
                        }else {
                            holder.getView(R.id.ll_bg).setBackgroundColor(Color.parseColor("#FFFFFF"));
                            ((TextView)holder.getView(R.id.tv_text)).setTextColor(Color.parseColor("#A6000000"));
                        }
                    }

                }else {

                }

                if(bindItemLayoutId==0 && popupInfo.isDarkTheme){
                    holder.<TextView>getView(R.id.tv_text).setTextColor(getResources().getColor(R.color._xpopup_white_color));
                }
            }
        };
        adapter.setOnItemClickListener(new MultiItemTypeAdapter.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                if (selectListener != null) {
                    selectListener.onSelect(position, adapter.getData().get(position));
                }
                if (checkedPosition != -1) {
                    checkedPosition = position;
                    adapter.notifyDataSetChanged();
                }
                if (popupInfo.autoDismiss) dismiss();
            }
        });
        recyclerView.setAdapter(adapter);
        if (bindLayoutId==0 && popupInfo.isDarkTheme){
            applyDarkTheme();
        }
    }

    @Override
    protected void applyDarkTheme() {
        super.applyDarkTheme();
        recyclerView.setBackgroundColor(getResources().getColor(R.color._xpopup_dark_color));
    }

    String[] data;
    int[] iconIds;

    public AttachListPopupView1 setStringData(String[] data, int[] iconIds) {
        this.data = data;
        this.iconIds = iconIds;
        return this;
    }

//    public AttachListPopupView setOffsetXAndY(int offsetX, int offsetY) {
//        this.defaultOffsetX += offsetX;
//        this.defaultOffsetY += offsetY;
//        return this;
//    }

    private OnSelectListener selectListener;

    public AttachListPopupView1 setOnSelectListener(OnSelectListener selectListener) {
        this.selectListener = selectListener;
        return this;
    }

    public int checkedPosition = -1;
    /**
     * 设置默认选中的位置
     *
     * @param position
     * @return
     */


}
