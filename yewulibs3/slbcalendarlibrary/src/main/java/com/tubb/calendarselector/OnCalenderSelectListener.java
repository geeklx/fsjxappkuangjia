package com.tubb.calendarselector;

import java.util.Calendar;

public interface OnCalenderSelectListener {
    void onCalenderSelect(Calendar startCalendar, Calendar endCalendar);
}
